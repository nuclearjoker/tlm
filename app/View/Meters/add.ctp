<?                                                                                                                                        
    $this->Html->addCrumb('Meters', '/meters');
    $this->Html->addCrumb('Add Meter', '');
?>
<div class="row-fluid">
<div class="span4">
    
    <?php echo $this->Form->create('Meter');?>
	<fieldset>
		<legend><i class="icon-dashboard"></i>  <?php echo __('Add New Meter'); ?></legend>
	<?php
		echo $this->Form->input('name', array('type'=>'text'));

		echo $this->Form->input('utility_type_id');
    ?>
       <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon-exclamation"></i> Utilities can be added under 
            <? echo $this->Html->link(__('Utility Types'), array('controller'=>'utility_types'), 
                                      array('class' => '') ); ?>
        </div>
    <?

        $default = array();
        if(isset($selected))
            $default = array('default'=>$selected);

		echo $this->Form->input('property_id',$default);
    ?>
    <?
		echo $this->Form->input('unit_id', array('empty' => true) );
	?>
	</fieldset>
    <?php echo $this->Form->end( array( 'label' => 'Add Meter',
                                        'class' => 'btn-small btn-success'
                                      )
                                );

    ?>

  </div>
 </div>
</div>

<?
//This generate jquery code to make units set by property_id
$this->Js->get('#MeterPropertyId')->event('change', 
    $this->Js->request(array(
                'controller'=>'units',
                'action'=>'getByProperty'
        ), array(
                'update'=>'#MeterUnitId',
                'async' => true,
                'method' => 'post',
                'dataExpression'=>true,
                'data'=> $this->Js->serializeForm(array(
                'isForm' => true,
                'inline' => true
            ))
        ))
    );
?>
