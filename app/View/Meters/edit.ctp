<?                                                                                                                                        
    $this->Html->addCrumb('Meters', '/meters');
    $this->Html->addCrumb('Edit Meter', '');
?>

<div class="row-fluid">
<div class="span4">
<?php echo $this->Form->create('Meter');?>
	<fieldset>
		<legend><i class="icon-dashboard"></i>  <?php echo __('Edit Meter'); ?></legend>
	    <?php
		echo $this->Form->input('name', array('type'=>'text'));

		echo $this->Form->input('utility_type_id');
        ?>
       <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon-exclamation"></i> More utilites can be added under 
            <? ?>
        </div>
        <?
        $default = array();
        if(isset($selected))
            $default = array('default'=>$selected);

		echo $this->Form->input('property_id',$default);
		echo $this->Form->input('unit_id', array('empty' => true) );
	   ?>
       <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon-exclamation"></i>  Adding a unit and is optional.
        </div>
	</fieldset>
    <?php echo $this->Form->end( array( 'label' => 'Commit Changes',
                                        'class' => 'btn-small btn-warning'
                                      )
                                );

    ?>

  </div>
 </div>
</div>

<?
//This generate jquery code to make units set by property_id
$this->Js->get('#MeterPropertyId')->event('change', 
    $this->Js->request(array(
                'controller'=>'units',
                'action'=>'getByProperty'
        ), array(
                'update'=>'#MeterUnitId',
                'async' => true,
                'method' => 'post',
                'dataExpression'=>true,
                'data'=> $this->Js->serializeForm(array(
                'isForm' => true,
                'inline' => true
            ))
        ))
    );
?>
