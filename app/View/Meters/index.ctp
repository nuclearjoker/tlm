<?
    $this->Html->addCrumb('Meters');
   // $this->Html->addCrumb('View Meter', '/_meters/view/'+$Meter['Meter']['id'] );
  
  echo $this->Html->script('flot/jquery.flot.js');
  echo $this->Html->script('flot/jquery.flot.selection.js');
?>

<script type="text/javascript">
    $(document).ready(function() {
        jQuery("#togglefilter").click( function(event) {
		 
                 if ( $("#filterpage").is(":visible") ) {
                   $("#filterpage").hide();
                  
                 } else {
                  
                   $("#filterpage").show();
                   $("#filterpage").fadeIn(700);
                 }
                event.preventDefault();
          } );
          
          jQuery("#sum").click( function(event) {
		  var sList = ""; sum = 0;
		    //$('input[type=checkbox]').each(function () {
		    $('.sumcheck').each(function () {
			var sThisVal = (this.checked ? "1" : "0");
			//sList += (sList=="" ? sThisVal : "," + sThisVal);
			if(sThisVal == "1")
			    sum += parseInt(this.value);
		    });
		    alert("Total sum:"+sum);
          } );
    });
</script>
<h1> <i class="icon-dashboard"></i> <?php echo __('Meters');  ?></h1>
<div class="row-fluid" id="maintable">
  <div class="span12 well">
  
  <!-- <a href="#" id="togglefilter" title="Toggle Filter" class="btn-small btn-primary">Toggle Filter</a> -->

 
<div class="row-fluid">
  <div class="span12 ">
  <?php echo $this->Html->link('<i class="icon-plus"></i> New Meter', array('action' => 'add'), array( 'class' => 'btn-small btn-success', 'escape' => false) ); ?>
   <a href="#" id="sum" title="Usage Sum" class="btn-small btn-inverse hidden-tablet hidden-phone">Usage Sum</a>
  </div>
</div>
      <table class="table table-hover table-striped table-condensed">
	<tr>
	    <th class=""><?php echo $this->Paginator->sort('name');?></th>
	    <th><?php echo $this->Paginator->sort('property_id');?> </th>
            <th><?php echo $this->Paginator->sort('unit_id');?> </th>
	    <th><?php  $thismonth = date("F");  echo $thismonth ?> </th>
            <th class="hidden-tablet hidden-phone">&Delta; Usage</th>
	    <th>Last Reading</th>
	    <th class="hidden-phone" >Date of Reading</th>
	    <th> </th>
	</tr>
	<?php
         
	  foreach ($Meters as $Meter): 
	?>

	<tr>
	<td> 
            <? echo $this->Html->link( $Meter['Meter']['name'] ,
                                        array('action' => 'view', $Meter['Meter']['id'])  ); ?>
        </td>
		<td><?php echo h($Meter['Property']['name']); ?>&nbsp;</td>
                <td><?php echo h($Meter['Unit']['name']); ?>&nbsp;</td>
		<td nowrap>
	      <?php
                
                //last reading column:
		if ( !empty($readings) && array_key_exists($Meter['Meter']['id'],$readings) ) {
		    echo round( $readings[$Meter['Meter']['id']] );
		    //$usage  = round( $readings[$Meter['Meter']['id']] , 2 );
		  } else {  //show quick_add option
               ?>
	            <form action="<?php echo $this->Html->url(array('controller'=>'_readings','action'=>'quick_add',$Meter['Meter']['id'])); ?>" 
				  id="ReadingAddForm" method="post" accept-charset="utf-8" class="from-inline" >
		      <input type="hidden" name="_method" value="POST"/>
		      <input name="data[Reading][units]" length="7" type="number" id="ReadingUnits" class="input-small"/>
		      <input  type="submit" value="Add" class="btn-small btn-inverse" />
		    </form> 
                    </td>
		<?php }


		  ?>
	      <td class="hidden-tablet hidden-phone">
                  <?
                  if(!empty($usage) && array_key_exists($Meter['Meter']['id'],$usage) ) {
                     echo '<input type="checkbox" value='.round($usage[$Meter['Meter']['id']],2).' class="sumcheck" />';
                     echo round($usage[$Meter['Meter']['id']],2);
                  } else {
                     echo "N/A";
                  }?>
	      </td> 
	      
	      <td>
		  <?if(!empty($last_readings) && array_key_exists($Meter['Meter']['id'],$last_readings) ) {
                        echo round($last_readings[$Meter['Meter']['id']],2);
                    } else {
                        echo "N/A";
                    }?>
	      </td>
	      <td class="hidden-phone" >
		  <?if(!empty($last_readings_date) && array_key_exists($Meter['Meter']['id'],$last_readings_date) ) {
                       echo $last_readings_date[$Meter['Meter']['id']];
                  } else {
                        echo "N/A";
                  }?>
              </td>
	      <td>
           <div class="btn-group">
			  <?php //echo $this->Html->link(__('Add Reading'), array('controller'=>'_readings','action' => 'add', $Meter['Meter']['id']), array('class'=>'btn')); ?>
			  <?php echo $this->Html->link('<i class="icon-search"></i>View', array('action' => 'view', 
                                           $Meter['Meter']['id']), array('class'=>'btn btn-small', 'escape'=>false) ); ?>
			  <?php echo $this->Html->link( '<i class="icon-edit"></i>Edit' , array('action' => 'edit', $Meter['Meter']['id']), 
                                            array('class'=>'btn btn-small', 'escape' => false )); ?>
			  <?php echo $this->Form->postLink(__('<i class="icon-remove"></i>Delete'), 
                                                array('action' => 'delete', $Meter['Meter']['id']), 
				                                array('class'=>'btn btn-small', 'escape' => false),
                                                 __('Are you sure you want to delete meter: %s?', $Meter['Meter']['name'])); ?>
            </div>
	      </td>
	</tr>
	<?php endforeach; ?>
    </table>
    <div class="pagination">
         <ul>
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
    </div>
    </div>

  </div>
  
</div>
  
  
<style>

td.legendLabel   {
  padding: 0px 0px 0px 0px;
  border-bottom: none;
  width: 0px;
}
td.legendColorBox   {
  width: 0px;
  padding: 0px 0px 0px 0px;
  border-bottom: none;
}
</style>

<!--
<div class="row-fluid hidden-phone">
  <div class="span10" >
    <h3> Monthly Usage </h3>
      <div id="placeholder"  style="width:80%;height:350px;padding-top: 0px"></div>
	<p><label><input id="zoom" type="checkbox" checked="checked"/>Zoom to selection.</label></p>
	<div id="clickdata">  Point:<span id="x">0</span> Units:<span id="y">0</span> </div>
      </div>
    </div>
-->
<? $i=0;
/*
  function sortByMonth($a, $b) {
        return $a['for_month'] - $b['for_month'];
  }

  $prevcount = 0;
    $xaxis = array();
  foreach ($Meters as $Meter) : 
    $readings =  $Meter["Reading"];
    $name =  $Meter["Meter"]["name"]." (".substr($Meter["Property"]["name"],0,7).")";
    // $outr["label"] = $Meter["Meter"]["name"];
    $j=0;
    $lastreading = 0;

    usort($readings, 'sortByMonth');

    $count = count($readings);
    $updateaxis = false;

    if ( $count > $prevcount ) {
      $prevcount = $count;
      $updateaxis = true;
    }

    foreach($readings as $reading) {
      $outr[$name][] = array( $j, intval($reading["units"]));
      if($updateaxis) {
        $xaxis[] = array( intval($j-1), date("M Y",$reading["for_month"]) );
       }
      ++$j;
    } 
    $i++;
  endforeach;

if( isset($outr) && count($outr) > 0 ) :
  foreach ( $outr as $k => $v ) {
    $i=0;
    $last = 0;
    $rr = array();
    foreach ($v as $cord) {
        if ($i > 0) {
          $usage = $cord[1]-$last;
          $rr[] = array($i-1,$usage);
        }
        $last = $cord[1];
        $i++;
     }
     $label[] = $k;
       $outout[] = "{ label:'".$k."' , data:". json_encode($rr)."}";
     // $outout[] = $rr;
  }
*/
  ?>

<script>


$(function () {
  /*  var xaxisval = <? echo json_encode($xaxis) ;?> ;


    var data = <? 
        //$outr[$name][] = array("color" => "#FF0000"); 
        echo str_replace("\"","",json_encode($outout)); ?> ; 

    var options = {
       xaxis: {
       }
    };

  var options = {
        series: {
            lines: { show: true , fill :false, steps: true},
       //   bars: { show:true },
       //   points: { show: true }
        },
        legend: { noColumns: 3,  backgroundOpacity: 0.25, position : "nw" },
        xaxis: {  ticks: xaxisval},
        yaxis: { min: 0 },
        selection: { mode: "x" },
        grid: {
          backgroundColor: { colors: ["#fff", "#ddd"] } ,
          hoverable: true, clickable: true 
        }
    };

    $.plot($("#placeholder"),  data , options);

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#fee',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

  var previousPoint = null;
    $("#placeholder").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

          if (item) {
              if (previousPoint != item.dataIndex) {
                  previousPoint = item.dataIndex;
                  
                  $("#tooltip").remove();
                  var x = item.datapoint[0].toFixed(2),
                      y = item.datapoint[1].toFixed(2);
                  
                  showTooltip(item.pageX, item.pageY,
                              item.series.label + " of " + x + " = " + y);
              }
        }
    });


 var placeholder = $("#placeholder");

    placeholder.bind("plotselected", function (event, ranges) {
        $("#selection").text(ranges.xaxis.from.toFixed(1) + " to " + ranges.xaxis.to.toFixed(1));

        var zoom = $("#zoom").attr("checked");
        if (zoom)
            plot = $.plot(placeholder, data,
                          $.extend(true, {}, options, {
                              xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
                          }));
    });

  $("#placeholder").bind("plotclick", function (event, pos, item) {
     if (item) {
         $("#clickdata").text( item.dataIndex + " in " + item.series.label + ".");
           plot.highlight(item.series, item.datapoint);
     }
   });    
  });
*/
</script>

<? //endif; ?>

