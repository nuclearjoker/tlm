<?
$this->Html->addCrumb('Meters', '/_meters/index');
$this->Html->addCrumb('View Meter');
//debug($Meter);
?>
<style>
.box_rotate {
     -moz-transform: rotate(90eg);  /* FF3.5+ */
       -o-transform: rotate(90deg);  /* Opera 10.5 */
  -webkit-transform: rotate(90deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
}
</style>
<script type="text/javascript">
$( document ).ready(function() {
          boxes = $('.top-row');
       maxHeight = Math.max.apply(
         Math, boxes.map(function() {
           return $(this).height();
       }).get());
       boxes.height(maxHeight+30);
       //for each element that is classed as 'pull-down', set its margin-top to the difference between its own height and the height of its parent
      $('.pull-down').each(function() {
          $(this).css('margin-top', $(this).parent().height()-$(this).height())
      });
});   
</script>
  

<h1><i class="icon-dashboard"></i> <?php echo h($Meter['Meter']['name']);?></h1>

  
<div class="row-fluid">
  <div class="span4 well  top-row">
     <h3> Details </h3>
       	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($Meter['Meter']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property Name'); ?></dt>
		<dd>
			<?php echo h($Meter['Property']['name']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo h($Meter['Unit']['name']); ?>
			&nbsp;
		</dd>
		<dt>&nbsp;</dt>
		<dd>
			&nbsp;
		</dd>
	</dl>
       <?php echo $this->Html->link(__('Edit Meter'), 
					 array('action' => 'edit', $Meter['Meter']['id']), array('class' => 'btn-small btn-warning') );     ?>
        <?php echo $this->Form->postLink(__('Delete Meter'), 
            			 array('action' => 'delete', $Meter['Meter']['id']), array('class' => 'btn-small btn-danger'),
	   							       __('Are you sure you want to delete # %s?', $Meter['Meter']['id'])); ?>
   </div> 
    
    
  <div class="span4 well  top-row" >
  <h3> Monthly Usage </h3>
  <b>Average: <?php  echo(round($average,2)); ?></b>
  <ul class="timeline text-center">
    <?php 
    if (!empty($usage) && count($usage)>1 ){
       $usage_arr = array_values($usage);
       $max = max($usage_arr);
       
       foreach ($Readings as $Reading) {
        $usagevalue = $usage[ $Reading['Reading']['id'] ];
         $date = date('M Y',strtotime($Reading['Reading']['for_month']));
        $usageperc = round( ($usagevalue/$max)*100 ); //get usage as percentage
        ?>   
        <li>
          <a href=" <?php echo $this->Html->url(array('controller'=>'readings','action'=>'view', $Reading['Reading']['id'] )); ?>" title="<?php echo $date." : ".$usagevalue;?>">
            <span class="label"> <? echo str_replace(' ','<br>',$date) ?> </span>
            <span class="count" style="height: <?php echo $usageperc; ?>%"></span>
          </a>
        </li>
       <?php 
      } //endforeach
    }
    ?>
    </ul>
  </div>
    
    
  <div class="span4 well  top-row" >

  <h3> Daily Usage </h3>
  <b>Average: <?php echo(round($daily_average_average,2)); ?></b>
  <ul class="timeline">
    <?php 
    if (!empty($daily_average) && count($daily_average)>1){
       $usage_arr = array_values($daily_average);
       $max = max($usage_arr);
       
    foreach ($Readings as $Reading) {
         $usagevalue = $daily_average[ $Reading['Reading']['id'] ];
         $date = date('M Y',strtotime($Reading['Reading']['for_month']));
         $usageperc = round( ($usagevalue/$max)*100 ); //get usage as percentage
         ?>
         <li>
           <a href=" <?php echo $this->Html->url(array('controller'=>'readings','action'=>'view', $Reading['Reading']['id'] )); ?>" title="<?php echo $date." : ".$usagevalue;?>">
             <span class="label "> <? echo str_replace(' ','<br>',$date) ?> </span>
             <span class="count" style="height: <?php echo $usageperc; ?>%"></span>
           </a>
         </li>
        <?php 
       } //endforeach
    }?>
   </ul>
  </div>
</div>

<div class="row-fluid">
  <div class="span12 well">
    <h3 class="pull-left"><?php echo __('Usage Breakdown');?></h3>
      
    <table class="table table-condensed table-hover">
	<tr>
            <th>Date of Reading </th>
            <th>Units </th>
            <th>Usage </th>
            <th>Daily Avg. </th>
            <th>Actions </th>
	</tr>
        <?php
         foreach ($Readings as $Reading): 
                $current_timestamp = strtotime($Reading['Reading']['for_month']); ?>
            <tr>
		<td><?php echo $Reading['Reading']['for_month'];?></td>
		<td><?php echo round($Reading['Reading']['units'],2); ?></td>
                <td><?php echo $usage[ $Reading['Reading']['id'] ];?> <td>
                <?php
                    echo $daily_average[ $Reading['Reading']['id'] ];
                ?>
            </td>
            <td>
                <div class="btn-group">
                            <?php echo $this->Html->link(__('View'), array('controller'=>'readings', 'action' => 'view', $Reading['Reading']['id']), array('class' => 'btn btn-small')); ?> 
                            <?php echo $this->Html->link(__('Edit'), array('controller'=>'readings', 'action' => 'edit', $Reading['Reading']['id']), array('class' => 'btn btn-small')); ?> 
                            <?php echo $this->Form->postLink(__('Delete'), array('controller'=>'readings', 'action' => 'delete', $Reading['Reading']['id']), array('class' => 'btn btn-small'),
                                                        __('Are you sure you want to delete reading # %s?', $Reading['Reading']['for_month'])); ?>
                </div>
            </td>
	</tr>
  

        <?php endforeach; ?>
	</table>
        <?php echo $this->Html->link(__('See All Readings'), 
                          array('controller'=>'readings','action' => 'index', 
                          $Meter['Meter']['id']), array('class' => 'btn-small btn-info ')); ?> &nbsp;
        <?php echo $this->Html->link(__('Add Reading'), 
                          array('controller'=>'readings','action' => 'add', 
                          $Meter['Meter']['id']),  array('class' => 'btn-small btn-success ')); ?>
     </div>
  </div>

