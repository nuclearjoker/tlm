
<?                                                                                                               
    $this->Html->addCrumb('Properties', '/houses');
    $this->Html->addCrumb('Property Edit', '');
?>

<div class="span12">
<?php echo $this->Form->create('Property');?>
	<fieldset>
		<legend><?php echo __('Edit Property'); ?></legend>
		<?php echo $this->Form->postLink(__('Delete This Property'), 
                                            array('action' => 'delete', $this->Form->value('Property.name')), 
                                            null, __('Are you sure you want to delete # %s?', 
                                            $this->Form->value('Property.name'))); ?>
	<?php
		echo $this->Form->input('name',array('type'=>'text','length'=>20));
		echo $this->Form->input('latitude',array('type'=>'text','length'=>20));
		echo $this->Form->input('longitude',array('type'=>'text','length'=>20));
		echo $this->Form->input('address',array('type'=>'textarea','length'=>20));
		//echo $this->Form->input('picture',array('type'=>'file'));
  		//echo $this->Form->input('picture', array('type'=>'text') );
	?>
     <?php echo $this->Form->end( array( 
                                        'label' => 'Submit Changes',
                                        'class' => 'btn btn-success'
                                    )
                                );
      ?>
	</fieldset>

</div>
