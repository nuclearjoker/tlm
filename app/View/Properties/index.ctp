<?                                                                                                               
    $this->Html->addCrumb('Properties', '');
?>
<div class="row-fluid">
  <div class="span12 well">
      <div class="row-fluid">
        <h1 class="tile-hot"> <i class="icon-building icon"></i> <?php echo __('Properties');?>
       <?php echo $this->Html->link(__('New Property'), 
                                         array('action' => 'add'), array('class' => 'btn-small btn-success pull-right')); ?>
        </h1>
      </div>
      <table class="table table-hover">
	<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th> <?php echo $this->Paginator->sort('address');?> </th>
                        <th>  </th>
	</tr>
        
        <?php foreach ($properties as $property): ?>
	<tr>
		<td><?php echo h($property['Property']['name']); ?></td>
                <td><?php if( strlen($property['Property']['address']) > 30 ) 
                                echo $property['Property']['address']; 
                           else
                                echo substr(str_replace(array('\r','\n'),array(',',','),$property['Property']['address']),0,30).' ...'; ?>
                    
                </td>
		<td>
            <div class="btn-group pull-right">
                <?php echo $this->Html->link(__('View'), array('action' => 'view', $property['Property']['id']),
                                                         array( 'class'=> 'btn btn-small') ); ?>
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $property['Property']['id']),
                                                         array( 'class'=> 'btn btn-small') ); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $property['Property']['id']), 
                                                               array( 'class'=> 'btn btn-small'), __('Are you sure you want to delete %s?', $property['Property']['name'])); ?>
            </div>
            </td>
	</tr>
        <?php endforeach; ?>
	</table>

     <div class="pagination">
    <ul>
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
    </div>


    </div>
    </div>
</div>

