<?
    $this->Html->addCrumb('Properties', '/properties');
    $this->Html->addCrumb('Property Add', '');
?>    

<div class="row-fluid">
 <div class="span12">
   <?php echo $this->Form->create('Property',array('type'=>'file','action'=>'add'));?>
    <div class="span5">
	<fieldset>
		<legend> <i class="icon-building icon-2x"></i> <?php echo __('Add Property'); ?></legend>
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <i class="icon-info"></i> This will be displayed as the primary for the property.
        </div>
		<? echo $this->Form->input('name',array('type'=>'text','length'=>20)); ?>
        

        <legend> Location Information </legend>
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon-exclamation"></i> These are optional, but could be useful later - especially if you would like to manage many properties.
        </div>
        <?
		echo $this->Form->input('address',array('type'=>'textarea'));
		echo $this->Form->input('longitude',array('type'=>'text','length'=>30));
		echo $this->Form->input('latitude',array('type'=>'text','length'=>30));

  //  echo $this->Form->input('picture', array('type'=>'file') );
        // Add upload picture.
	    ?>
	</fieldset>
<?php echo $this->Form->end(__('Add Property'));?>
    </div>
   </div>
</div>
