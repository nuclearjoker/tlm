<?
    // include the Google js code
    echo $this->Html->script($this->GoogleMapV3->apiUrl());
    $this->Html->addCrumb('Properties', '/properties');
    $this->Html->addCrumb('Property View', '');
?>    

<div class="row-fluid well">
   
   <div class="span4 pull-left">
       <h2> <i class="icon-building icon"></i>  <?php echo "&nbsp;" ;echo h($property['Property']['name']);?></h2>
        <fieldset>
            <dt><?php echo __('Name'); ?></dt>
            <dd>
                <?php echo h($property['Property']['name']); ?>
                &nbsp;
            </dd>
            <dt> Longitude </dt>
            <dd>
                <?php echo h($property['Property']['longitude']); ?>
                &nbsp;
            </dd>
            <dt> Latitude </dt>
            <dd>
                <?php echo h($property['Property']['latitude']); ?>
                &nbsp;
            </dd>
            <dt> Address </dt>
            <dd>
             <pre class="prettyprint"><?php echo $property['Property']['address']; ?></pre>
            </dd>
            <dt><?php //echo __('Picture'); ?></dt>
            <dd>
            <?php /* if(!empty($property['Property']['picture'])) 
                    echo $this->Html->image($property['Property']['picture']
                            , array('alt' => ($property['Property']['picture'])
                            ,'width'=>'348' ) ); */ ?> 

                &nbsp;
            </dd>
        </dl>
        </fieldset>
         <div class="form-actions">
        <?php echo $this->Html->link(__('Edit Property'), array('action' => 'edit', $property['Property']['id']),
                                                array( 'class'=> ' btn-small btn-warning') ); ?>
       <?php echo $this->Form->postLink(__('Delete Property'), array('action' => 'delete', $property['Property']['id']), 
                                                      array( 'class'=> ' btn-small btn-danger'), __('Are you sure you want to delete %s?', $property['Property']['name'])); ?>
        </div>
     </div>
    <div class="span8 pull-right">
      <? 
        // init map (prints container)
        // tip: use it inside a for loop for multiple markers
        echo $this->GoogleMapV3->map(
                array('div'=>array('height'=>'400', 'width'=>'100%'),
                      'lat' =>   $property['Property']['latitude'],
                      'lng' =>  $property['Property']['longitude'],
                      'type' => 'H',
                      'zoom' => 17
                     )
                );
        // add markers
        $options = array(
            'lat' =>   $property['Property']['latitude'],
            'lng' =>  $property['Property']['longitude'],
            //'icon'=> '', // optional
            'title' => $property['Property']['name'], // optional
            'content' => '<i>'.$property['Property']['address'].'</i>'// optional
        );
        $this->GoogleMapV3->addMarker($options);
        echo $this->GoogleMapV3->script();
     ?>
    </div>
    
    </div>

<div class="row-fluid well">
    <div class="span12">
        <?php if (!empty($property['Unit'])):?>
         <div class="container">
            <div class="row-fluid">
               <?php echo $this->Html->link(__('New Unit'), array('controller' => 'units', 'action' => 'add', $property['Property']['id']), array('class'=>'btn-small btn-success pull-right') );?>
                <h3 >Units</h3>
            </div>
        </div>
        <table class="table  table-hover table-condensed">
	<tr>
		<th><?php echo __('Name'); ?></th>
		<th class="actions"><?php //echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($property['Unit'] as $unit): ?>
		<tr>
			<td><?php echo $unit['name'];?></td>
			<td ><div class="btn-group pull-right">
                            
				<?php echo $this->Html->link(__('View'), array('controller' => 'units', 'action' => 'view', $unit['id']), array('class' => 'btn btn-small') ); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'units', 'action' => 'edit', $unit['id']), array('class' => 'btn btn-small') ); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'units', 'action' => 'delete', $unit['id']), array('class' => 'btn btn-small'), __('Are you sure you want to delete # %s?', $unit['id'])); ?>
                            </div></td>
		</tr>
	<?php endforeach; ?>
	</table>
   <?php endif; ?>
  </div>
 </div>


<div class="row-fluid well">
    <div class="span12">
        <div class="row-fluid">
            <h3 style="float:left;text-align:left" >Meters</h3>

            <?php echo $this->Html->link(__('New Meter'), 
                          array('controller' => 'meters', 'action' => 'add', $property['Property']['id'] ), array('class'=>'btn-small btn-success pull-right'));?> 
        </div>
	<?php if (!empty($property['Meter'])):?>
	<table class="table table-hover table-condensed">
	<tr>
		<th><?php echo __('Name'); ?></th>
		<th class="actions"><?php //echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($property['Meter'] as $Meter):
			$Meter =  $Meter['Meter'];
		?>

		<tr>
			<td><?php echo $Meter['name'];?></td>
			<td ><div class="btn-group pull-right">
                            
				<?php echo $this->Html->link(__('View'), array('controller' => 'meters', 'action' => 'view', $Meter['id']), array('class' => 'btn btn-small')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'meters', 'action' => 'edit', $Meter['id']), array('class' => 'btn btn-small')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'meters', 'action' => 'delete', $Meter['id']) , array('class' => 'btn btn-small'), __('Are you sure you want to delete # %s?', $Meter['id'])); ?>
                            </div></td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

    </div>
 </div>


