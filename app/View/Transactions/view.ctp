<?php
debug($transactions);
?>
<div class="transactions view">
<h2><?php
        echo __('Transaction for');
        //echo $transaction['Tenant']['name'];
    ?>
</h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Transaction Type'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['transaction_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($transaction['Tenant']['name'], array('controller' => 'tenants', 'action' => 'view', $transaction['Tenant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Detail'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['detail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Units'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['units']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost Per Unit'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['cost_per_unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Transaction'), array('action' => 'edit', $transaction['Transaction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Transaction'), array('action' => 'delete', $transaction['Transaction']['id']), null, __('Are you sure you want to delete # %s?', $transaction['Transaction']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tenants'), array('controller' => 'tenants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tenant'), array('controller' => 'tenants', 'action' => 'add')); ?> </li>
	</ul>
</div>
