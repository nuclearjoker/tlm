<div class="transactions form">
<?php echo $this->Form->create('Transaction');?>
	<fieldset>
		<legend><?php echo __('Add Transaction'); ?></legend>
	<?php
		echo $this->Form->input('transaction_type');
		echo $this->Form->input('cost');
		echo $this->Form->input('tenant_id');
		echo $this->Form->input('detail');
		echo $this->Form->input('units');
		echo $this->Form->input('cost_per_unit');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Transactions'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Tenants'), array('controller' => 'tenants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tenant'), array('controller' => 'tenants', 'action' => 'add')); ?> </li>
	</ul>
</div>
