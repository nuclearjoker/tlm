<?
function outR($number) {
    return number_format((float)$number, 2, '.', '');
}

?>
<div class="table table-hover">
	<h2><?php echo __('Transactions');?></h2>
	<table >
	<tr>
			<th><?php echo $this->Paginator->sort('date');?></th>
			<th><?php echo $this->Paginator->sort('tenant');?></th>
			<th><?php echo $this->Paginator->sort('transaction_type');?></th>
			<th><?php echo $this->Paginator->sort('detail');?></th>
			<th><?php echo $this->Paginator->sort('units');?></th>
			<th><?php echo $this->Paginator->sort('cost_per_unit');?></th>
			<th><?php echo $this->Paginator->sort('cost');?></th>
			<th>Balance</th>
            <th> </th>
	</tr>
    <?php
         $balance = 0;
	foreach ($transactions as $transaction): 
            $type = "N/A";
            if ( $transaction['Transaction']['transaction_type'] == -1) {
                  $type = "Debit"; 
            } else if ( $transaction['Transaction']['transaction_type'] == 1) {
                  $type = "Credit";
            }
            $balance += $transaction['Transaction']['cost']*$transaction['Transaction']['transaction_type']; 
            
    ?>
	<tr>
		<td><?php echo h($transaction['Transaction']['date']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($transaction['Tenant']['name'], array('controller' => 'tenants', 'action' => 'view', $transaction['Tenant']['id'])); ?>
		</td>
		<td><?php echo h($type); ?>&nbsp;</td>
		<td><?php echo h($transaction['Transaction']['detail']); ?>&nbsp;</td>
		<td><?php echo h($transaction['Transaction']['units']); ?>&nbsp;</td>
		<td> R<?php echo h($transaction['Transaction']['cost_per_unit']); ?>&nbsp;</td>
		<td> R<?php echo h($transaction['Transaction']['cost']); ?>&nbsp;</td>
        <td style="white-space:nowrap" > R <?php echo h(outR($balance)); ?> </td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $transaction['Transaction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $transaction['Transaction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $transaction['Transaction']['id']), null, __('Are you sure you want to delete # %s?', $transaction['Transaction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

<li><?php echo $this->Html->link(__('List Tenants'), array('controller' => 'tenants', 'action' => 'index')); ?> </li>
