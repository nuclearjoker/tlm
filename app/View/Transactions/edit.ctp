<div class="transactions form">
<?php echo $this->Form->create('Transaction');?>
	<fieldset>
		<legend><?php echo __('Edit Transaction'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('transaction_type',array('type'=>'text'));
		echo $this->Form->input('cost',array('type'=>'text'));
		echo $this->Form->input('tenant_id');

		echo $this->Form->input('detail',array('type'=>'text'));
		echo $this->Form->input('units',array('type'=>'text'));
		echo $this->Form->input('cost_per_unit',array('type'=>'text'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Transaction.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Transaction.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Tenants'), array('controller' => 'tenants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tenant'), array('controller' => 'tenants', 'action' => 'add')); ?> </li>
	</ul>
</div>
