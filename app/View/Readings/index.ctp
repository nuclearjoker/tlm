<?
    //debug($Readings);
?>
<div class="Readings ">
	<h2><?php echo __('Readings');?></h2>
 
	
  <table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('for_month');?></th>
			<th><?php echo $this->Paginator->sort('units');?></th>
			<th><?php echo $this->Paginator->sort('_meter_id');?></th>
			<th><?php echo $this->Paginator->sort('House');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($Readings as $Reading): ?>
	<tr>
		<td><?php echo h($Reading['Reading']['for_month']); ?>&nbsp;</td>
		<td><?php echo h(round($Reading['Reading']['units'],2)); ?>&nbsp;</td>
		<td><?php echo h($Reading['Meter']['name']); ?>&nbsp;</td>
		<td><?php echo h($Reading['Meter']['Property']['name']); ?>&nbsp;</td>

		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $Reading['Reading']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $Reading['Reading']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $Reading['Reading']['id']), null, __('Are you sure you want to delete # %s?', $Reading['Reading']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>
	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

  <div class="actions"> 
     <?php if(isset($meterid))
            echo $this->Html->link(__('Back to Meter'), array('controller' => '_meters', 'action' => 'view',$meterid)); 
            ?> 
 </div>
<div class="actions"> 
<?php echo $this->Html->link(__('New Reading'), array('action' => 'add',$meterid)); ?>
</div>
