<?
    $this->Html->addCrumb('Meters', '/meters/index');
    $this->Html->addCrumb('View Reading','');
?>
<div class="well">
<h2><i class="icon-user"></i><?php  echo ' Reading ('.h($Reading['Meter']['name']).')'; ?></h2>
	<dl >
            	<dt><?php echo __('For the month of '); ?></dt>
		<dd>
			<?php echo h( date("F Y", strtotime($Reading['Reading']['for_month']) ) ); ?>
			&nbsp;
		</dd>
                <dt><?php echo __(' Meter'); ?></dt>
		<dd>
			<?php echo h($Reading['Meter']['name']); ?>
			&nbsp;
		</dd>

                <dt><?php echo __('Date Taken'); ?></dt>
		<dd>
			<?php echo h( $Reading['Reading']['date_taken']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Units'); ?></dt>
		<dd>
			<?php echo round($Reading['Reading']['units'],2); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo h($Reading['Meter']['Property']['name']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo h($Reading['Meter']['Unit']['name']); ?>
			&nbsp;
		</dd>
	</dl>
<?php echo $this->Html->link(__('Edit This Reading'), array('action' => 'edit', $Reading['Reading']['id']), array('class' => 'btn-small btn-warning') ); ?> &nbsp;
<?php echo $this->Form->postLink(__('Delete This Reading'), array('action' => 'delete', $Reading['Reading']['id']),
                                          array('class' => 'btn-small btn-danger'), __('Are you sure you want to delete # %s?', $Reading['Reading']['id'])); ?> 

</div>

