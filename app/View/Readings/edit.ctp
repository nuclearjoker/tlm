<div class="Readings form">
<?php echo $this->Form->create('Reading');?>
	<fieldset>
		<legend><?php echo __('Edit  Reading'); ?></legend>
	<?php
		echo $this->Form->input('units',array('type'=>'text'));
		echo $this->Form->input('for_month',array('type'=>'date'));
		echo $this->Form->input('meter_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Save Reading'));?>
<br>
<div style="padding-left: 0.5em;" >
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Reading.id')), 
    array('class'=>"actiondel") , __('Are you sure you want to delete this reading, # %s?', $this->Form->value('Reading.id'))); ?>
 </div>
</div>
