<div class="span12">
<?php echo $this->Form->create('Reading');?>
	<fieldset>
		<legend><?php echo __('Add Reading'); ?></legend>
	<?php
		echo $this->Form->input('units',array('type'=>'text','length'=>20));
		$default = array();
	       	if (isset($selected_id))
                $default = array('default' => $selected_id);

                echo $this->Form->input('meter_id', $default);
		echo $this->Form->input('for_month',array('type'=>'date'));
                echo $this->Form->input('date_taken',array('type'=>'date'));
	?>
	</fieldset>
<?php echo $this->Form->end( array('label'=>'Add Reading', 
                                    'class' => 'btn-small btn-success' ) 
                            );
?>
</div>
