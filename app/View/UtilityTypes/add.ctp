<div class="row-fluid">
  <div class="span12">
    <?php echo $this->Form->create('UtilityType');?>
        <fieldset class="span4">
            <legend><?php echo __('Add Utility Type'); ?></legend>
         <? echo $this->Form->input('name', array('type'=>'text') ); ?>
         <? echo $this->Form->input('unit', array('type'=>'text') ); ?>
           <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <i class="icon-exclamation"></i>  This is the unit of measurement for the utility (e.g. KwH).
            </div>

        <?  echo $this->Form->input('costperunit', array('type'=>'text', 'label' => 'Cost Per Unit') ); ?>
           <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <i class="icon-exclamation"></i> The is cost of one unit of measurement.
            </div>
        <? echo $this->Form->input('unit_description', array('type'=>'text')); ?>
           <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <i class="icon-exclamation"></i> Full wording of the unit of measurement (e.g. Kilowatt per Hour)
            </div>
        <?php echo $this->Form->end( array(
                    'label' => 'Add Utility Type',
                    'class' => 'btn-small btn-success' 
                    )
                );?>
        </fieldset>
  </span>
</div>
