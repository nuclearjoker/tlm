<div class="row-fluid">
  <div class="span12">
    <?php echo $this->Form->create('UtilityType');?>
        <fieldset class="span4">
            <legend><?php echo __('Edit Utility Type'); ?></legend>
        <? echo $this->Form->input('name', array('type'=>'text') ); ?>
        <? echo $this->Form->input('unit', array('type'=>'text') ); ?>
        <?  echo $this->Form->input('costperunit', array('type'=>'text', 'label' => 'Cost Per Unit') ); ?>
        <? echo $this->Form->input('unit_description', array('type'=>'text')); ?>
        <?php echo $this->Form->end( array(
                    'label' => 'Apply changes to utility.',
                    'class' => 'btn-small btn-success' 
                    )
                );?>
        </fieldset>
  </span>
</div>
