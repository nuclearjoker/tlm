<div class="row-fluid">
  <div class="span12">
	<h2><?php echo __('Utility Types');?></h2>

    <div class="btn-toolbar">
        <?php echo $this->Html->link(__('<i class="icon-plus"></i> New Utility Type'), array('action' => 'add'), 
                                         array('class'=>'btn-small btn-success','escape'=>false) 
                                     ); ?>
    </div>

	<table class="table table-hover">
	<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('costperunit');?></th>
			<th class="actions"><?php  echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($utilityTypes as $utilityType): ?>
	<tr>
		<td>
            <?php echo $this->Html->link($utilityType['UtilityType']['name'], 
                                         array('action' => 'view',$utilityType['UtilityType']['id']) 
                                        ); ?> 
        </td>

		<td><?php echo $this->Number->currency( $utilityType['UtilityType']['costperunit'] ); ?>
        </td>
		<td >
            <div class="btn-group">
            <?php echo $this->Html->link( __('View'), array('action' => 'view',$utilityType['UtilityType']['id']),
                                                      array('class' => 'btn btn-small btn-inverse') 
                                        ); 
                    ?>
            <?php echo $this->Html->link( __('Edit'), array('action' => 'edit', $utilityType['UtilityType']['id']),
                                                      array('class' => 'btn btn-small btn-inverse')
                                            ); 
                    ?>
            <?php echo $this->Form->postLink( __('Delete'), array('action' => 'delete', $utilityType['UtilityType']['id']), 
                                                           array('class' => 'btn btn-small btn-inverse'),
                                                            __('Are you sure you want to delete # %s?', $utilityType['UtilityType']['id'])
                                            ); 
                    ?>
            </div>
		</td>
	</tr>
    <? endforeach; ?>
    </table>
	<div class="row-fluid">
        <?php
 
                //echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                //echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
        <div style="text-align:left;float:left;color:#003D4C;"><b>
	<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page}/{:pages} ')
		));
	?>  </b>
    </div>
</div>
</div>

