<?
    $this->Html->addCrumb('Utility Types', '/utility_types');
    $this->Html->addCrumb('View Utility', '');
    //debug($utilityType);
?>

<div class="row-fluid">
<div class="span12">
<h2><?php  echo __('Utility Type:'); echo h($utilityType['UtilityType']['name']);?></h2>

		<?php echo $this->Html->link(__('Edit Utility Type'), 
                                     array('action' => 'edit', $utilityType['UtilityType']['id']), array('class'=>'btn-small btn-warning') ); ?> 
		<?php echo $this->Form->postLink(__('Delete Utility Type'), 
                                         array('action' => 'delete', $utilityType['UtilityType']['id']), array('class'=>'btn-small btn-danger'),
                                          __('Are you sure you want to delete # %s?', $utilityType['UtilityType']['name'])); ?> 
		<br>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($utilityType['UtilityType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost Per Unit'); ?></dt>
		<dd>
			<?php echo h($utilityType['UtilityType']['costperunit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo h($utilityType['UtilityType']['unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit Description'); ?></dt>
		<dd>
			<?php echo h($utilityType['UtilityType']['unit_description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
</div>
