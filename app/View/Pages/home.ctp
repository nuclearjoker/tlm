<?php


?>
<h2> Home </h2>

<div class="row-fluid">
 <div class="span12 well">
     <p> Welcome to the letting manager.</p>
    <div class="row-fluid">
       <div class="span4">
        <div class="tile trans">
          <?php echo $this->Html->image('home.png', array('alt' => '(Properties)','class' => 'tile-image big-illustration'))?>
          <h3 class="tile-title"> Properties </h3>
          <p> Your properties. </p>
          <div class="btn-group btn-group-vertical">
            <? echo $this->Html->link('<i class="icon-building"></i> View Properties', '/properties' , 
                                      array( 'class' => 'btn btn-info', 'escape' => false ) ); ?> 
            <? echo $this->Html->link('<i class="icon-home"></i> View Units', '/units' , 
                                      array( 'class' => 'btn btn-inverse', 'escape' => false ) ); ?>
         </div>
        </div>
      </div>
       <div class="span4">
        <div class="tile trans">
            <p class="tile-image">
              <i class="icon-male icon-4x tile-image big-illustration"></i>
              <i class="icon-female icon-4x"></i>
            </p>
          <h3 class="tile-title"> Tenants </h3>
          <p> See all your tenants here.</p>
          <div class="btn-group btn-group-vertical">
            <? echo $this->Html->link('<i class="icon-user"></i> View Tenants', '/tenants' , 
                                      array( 'class' => 'btn btn-info', 'escape' => false) ); ?> 
            <? echo $this->Html->link('<i class="icon-file-text-alt"></i> View Leases', '/leases' , 
                                      array( 'escape'=>false, 'class' => 'btn btn-primary') ); ?> 
            <? echo $this->Html->link('<i class="icon-plus"></i> New Tenant', '/tenants/add' , 
                                      array( 'class' => 'btn btn-inverse', 'escape' => false ) ); ?>
         </div>
         </div> 
       </div> 
       <div class="span4">
        <div class="tile trans">
          <?php echo $this->Html->image('time.png', array('alt' => '(Meters)','class' => 'tile-image big-illustration'))?>
          <h3 class="tile-title"> Meters </h3>
          <p> Your meters for properties. </p>
        
         <div class="btn-group btn-group-vertical"> 
          <? echo $this->Html->link('<i class="icon-dashboard"></i> View Meters', '/meters' , 
                                    array( 'class' => 'btn btn-info', 'escape' => false ) ); ?>
          <? echo $this->Html->link('<i class="icon-bolt"></i> New Meter', '/meters/add' , 
                                    array( 'class' => 'btn btn-inverse', 'escape' => false) ); ?>
         </div>
        </div> 
      </div>
    </div>

     
    <? /*
    <div class="row-fluid">
       <div class="span4">
        <div class="tile trans">
          <?php echo $this->Html->image('clipboard.png', array('alt' => '(Alerts)','class' => 'tile-image big-illustration'))?>
          <h3 class="tile-title"> Alerts and Notes </h3>
          <p> Your alerts and notifications. </p>
         <div class="btn-group btn-group-vertical"> 
          <? echo $this->Html->link('View Alerts', '/alerts' , array( 'class' => 'btn btn-info') ); ?>
         </div>
        </div> 
     </div>
   </div> */ ?>

 </div>
</div>