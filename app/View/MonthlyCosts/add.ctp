<?
	echo $this->Html->script('jquery.monthpicker.js')
?>
<script type="text/javascript">
    $(document).ready(function() {
	var oneYr = new Date();
        var now = new Date();
        oneYr.setYear(now.getFullYear() + 1);
       
        $('#MonthlyCostDateFrom').val(now.getFullYear()+'-'+now.getMonth());
        $('#MonthlyCostDateTo').val(oneYr.getFullYear()+'-'+oneYr.getMonth());
        
        $('#MonthlyCostDateFrom').monthpicker({ pattern:'yyyy-mm', selectedYear: now.getFullYear(),
                                                startYear: (now.getFullYear()-5),
                                                endYear:  (now.getFullYear()+5)
                                            } );
        $('#MonthlyCostDateTo').monthpicker(  { pattern:'yyyy-mm', selectedYear: oneYr.getFullYear(), 
                                                startYear: (now.getFullYear()-5),
                                                endYear:  (now.getFullYear()+5)
                                            } );

        

//        $('#MonthlyCostDateTo').;
        

        $('#MonthlyCostMeterId, label=[for="MonthlyCostMeterId"]').hide();
        $('#MonthlyCostUtilityTypeId, label=[for="MonthlyCostUtilityTypeId"]').show();
        $('#MonthlyCostUnits, label=[for="MonthlyCostUnits"]').show();
         $('#MonthlyCostFractionOfCost, label=[for="MonthlyCostFractionOfCost"]').hide();
         
        $("#MonthlyCostMeteredCost").change(function() {
                $("#MonthlyCostMeteredCost option:selected").each(function () {
                               if ($(this).text()=="Metered"){
                                   $('#MonthlyCostMeterId, label=[for="MonthlyCostMeterId"]').show();
                                   $('#MonthlyCostUtilityTypeId, label=[for="MonthlyCostUtilityTypeId"]').hide();
                                   $('#MonthlyCostUnits, label=[for="MonthlyCostUnits"]').hide();
                                   $('#MonthlyCostFractionOfCost, label=[for="MonthlyCostFractionOfCost"]').show();
                               }else{
                                   $('#MonthlyCostUtilityTypeId, label=[for="MonthlyCostUtilityTypeId"]').show();
                                   $('#MonthlyCostMeterId, label=[for="MonthlyCostMeterId"]').hide();
                                   $('#MonthlyCostUnits, label=[for="MonthlyCostUnits"]').show();
                                   $('#MonthlyCostFractionOfCost, label=[for="MonthlyCostFractionOfCost"]').hide();
				   $("#MonthlyCostMeterId").val('');
                               }
                       });
                }
	);
    });
</script>

    
  <h3><?php echo __('Add monthly cost'); 
                      if(count($tenants)==1) {//is specific addition of monthly cost.
                          echo h(" for ".reset($tenants));
                      }
                ?>
        </h3>

  <div class="row-fluid">
    <div class="span6">

        <?php echo $this->Form->create('MonthlyCost');?>
        <fieldset>

	<?php
                echo $this->Form->input('tenant_id');
                echo $this->Form->input('name',array('label'=> 'Name of cost (optional)', 'type'=>'text'));
         ?>
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon-exclamation"></i> 
            The date range below specify the months the monthly cost has to be deducted. From and to months are both <b>inclusive</b>.
        </div>
           <?         
                echo $this->Form->input('date_from', array('type'=>'text', 'class' => 'datefield') );
                echo $this->Form->input('date_to', array('type'=>'text', 'class' => 'datefield') );
          ?>
            
            </fieldset>
      </div>
      <div class="span6">
            <fieldset>
          <?    echo $this->Form->input('metered_cost', array(
                              'type'=>'select', 
                              'options' => array( 'Unmetered','Metered'))
                );
                echo $this->Form->input('utility_type_id', array('empty' => true ));
                //echo $this->Form->input('utility_type_id');
                echo $this->Form->input('meter_id', array('empty' => true )); //only list meters for the tenants property.
                echo $this->Form->input('fraction_of_cost', array('type'=>'text', 'default' => '1.0') );
                echo $this->Form->input('units',array('label' => 'Number of units (per month)', 'default' => 1));

          ?>
             </fieldset>
          
     </div>


   </div>
<div class="row-fluid">
    <div class="span6">
        <?php echo $this->Form->end( array('label'=>'Add Monthly Cost','class'=>'btn-small btn-success'));?>
   </div>
</div>
