<div class="monthlyCosts">
	<h2><?php echo __('Monthly Costs');?></h2>
     <div style="float:right;padding-top:16px">
          <p class="actions">
          <li> <?php echo $this->Html->link(__('New Monthly Cost'), array('action' => 'add')); ?></li>
          </p>
     </div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('tenant_id');?></th>
			<th><?php echo $this->Paginator->sort('fraction_of_cost');?></th>
			<th><?php echo $this->Paginator->sort('utility_type_id');?></th>
			<th class="actions"><?php //echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($monthlyCosts as $monthlyCost): ?>
	<tr>
		<td><?php echo h($monthlyCost['MonthlyCost']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($monthlyCost['Tenant']['name'], array('controller' => 'tenants', 'action' => 'view', $monthlyCost['Tenant']['id'])); ?>
		</td>
		<td><?php echo h($monthlyCost['MonthlyCost']['fraction_of_cost']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($monthlyCost['UtilityType']['name'], array('controller' => 'utility_types', 'action' => 'view', $monthlyCost['UtilityType']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $monthlyCost['MonthlyCost']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $monthlyCost['MonthlyCost']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $monthlyCost['MonthlyCost']['id']), null, __('Are you sure you want to delete # %s?', $monthlyCost['MonthlyCost']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
<div class="paging">
        <?php
 
                echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
        <div style="text-align:left;float:left;color:#003D4C;"><b>
     <?php
     echo $this->Paginator->counter(array(
     'format' => __('Page {:page}/{:pages} ')
     ));
     ?>  </b>
    </div>
</div>
