
<div class="row-fluid">
    <div class="span12 well">
    <h2><?php echo __('Monthly Cost'); ?></h2>
    <dl>
        <dt><?php echo __('Tenant'); ?></dt>
        <dd>
            <?php echo $this->Html->link($monthlyCost['Tenant']['name'], array('controller' => 'tenants', 'action' => 'view', $monthlyCost['Tenant']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Cost Name'); ?></dt>
        <dd>
            <?php echo h($monthlyCost['MonthlyCost']['name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __(' Meter'); ?></dt>
        <dd>
            <?php echo $this->Html->link($monthlyCost['Meter']['name'], array('controller' => '_meters', 'action' => 'view', $monthlyCost['Meter']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Fraction Of Cost'); ?></dt>
        <dd>
            <?php echo h( round($monthlyCost['MonthlyCost']['fraction_of_cost'],2)  ); ?>
            &nbsp;
        </dd>
        <? if( !empty($monthlyCost['UtilityType']['name'])): ?>
        <dt><?php echo __('Utility Type'); ?></dt>
        <dd>
            <?php echo $this->Html->link($monthlyCost['UtilityType']['name'], array('controller' => 'utility_types', 'action' => 'view', $monthlyCost['UtilityType']['id'])); ?>
            &nbsp;
        </dd>
        <? endif ?>
    </dl>
    </div>
</div>
 

<?php echo $this->Html->link(__('Edit Monthly Cost'), 
        array('action' => 'edit', $monthlyCost['MonthlyCost']['id'], $monthlyCost['Tenant']['id'] ), array('class'=>'btn-small btn-warning')); ?> 
<?php echo $this->Form->postLink(__('Delete Monthly Cost'), 
        array('action' => 'delete', $monthlyCost['MonthlyCost']['id']), 
         array('class'=>'btn-small btn-danger'), __('Are you sure you want to delete # %s?', $monthlyCost['MonthlyCost']['id'])); ?>
