<?
    $tenantid = "";
    if( !empty($Tenant) ) {
        $tenantid = $Tenant["Tenant"]["id"];
        $this->Html->addCrumb('Leases', '/leases');
        $this->Html->addCrumb('Specific Leases', '');
    } else 
        $this->Html->addCrumb('Leases', '');
    //debug( $leases   );
?>
<div class="row-fluid">
  <div class="span12">
	<h2>  <i class="icon-file-text-alt icon-2x"></i>  <?php echo __('Leases');
              if( !empty($Tenant) )
                    echo ": ".$Tenant["Tenant"]["name"]." ".$Tenant["Tenant"]["surname"];
        ?>
    </h2>

    <div class="btn-toolbar">
        <?php echo $this->Html->link(__('New Lease'), 
                                    array('action' => 'add', $tenantid), 
                                    array('class'=>'btn-small btn-success') ); ?>
    </div>

	<table class="table" cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Tenant');?></th>
			<th><?php echo $this->Paginator->sort('Unit');?></th>
			<th><?php echo $this->Paginator->sort('Lease Type');?></th>
			<th><?php echo $this->Paginator->sort('date_from');?></th>
			<th><?php echo $this->Paginator->sort('date_to');?></th>
			<th><?php echo $this->Paginator->sort('due');?></th>
			<th class="actions"><?php  echo __('Actions');?></th>
	</tr>
	<?php
    if( count($leases) == 0 ) {
        echo "<tr><td> No Leases created yet. </td><td></td><td></td><td></td><td></td><td></td><tr>";
    } else
	    foreach ($leases as $Lease): 
        ?>
	<tr>
        <td>
                <? echo $this->Html->link($Lease["Tenant"]["full_name"], 
                                           array('controller'=>'tenants',
                                                 'action' => 'view',$Lease['Tenant']['id']
                                                 )
                                         ); 
                ?>
        </td>
        <td>
                <? echo $Lease["Unit"]["name"]; ?>
        </td>
        <td>
                <? echo $Lease["LeaseType"]["display_type"]; ?>
        </td>
        <td>
                <? echo $this->Time->format($Lease["Lease"]["date_from"], "%Y-%m-%d"); ?>
        </td>
        <td>
                <? echo $this->Time->format($Lease["Lease"]["date_to"], "%Y-%m-%d"); ?>
        </td>
        <td>
                <? echo $this->Number->currency($Lease["Lease"]["due"]); ?>
        </td>
		<td >
            <div class="btn-group">
                <?php echo $this->Html->link( __('View'), array('action' => 'view',$Lease['Lease']['id']),
                                                          array('class' => 'btn btn-small btn-inverse') 
                                            ); 
                        ?>
                <?php echo $this->Html->link( __('Edit'), array('action' => 'edit', $Lease['Lease']['id']),
                                                          array('class' => 'btn btn-small btn-inverse')
                                                ); 
                        ?>
                <?php echo $this->Form->postLink( __('Delete'), array('action' => 'delete', $Lease['Lease']['id']), 
                                                               array('class' => 'btn btn-small btn-inverse'),
                                                                __('Are you sure you want to delete # %s?', $Lease['Lease']['id'])
                                                ); 
                        ?>
            </div>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<div class="paging">
        <?php
 
                echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
        <div style="text-align:left;float:left;color:#003D4C;"><b>
	<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page}/{:pages} ')
		));
	?>  </b>
    </div>
</div>
</div>

