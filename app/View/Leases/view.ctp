<?
    $this->Html->addCrumb('Leases', '/leases');
    $this->Html->addCrumb('Lease View', '');
    //debug( $Lease );
?>
<div class="span12">
<h2> <i class="icon-file-text-alt icon-2x"></i> <?php  echo __(' Lease');?></h2>
	<dl>
		<dt><?php echo __('Tenant'); ?></dt>
		<dd>
	                <?php echo $this->Html->link( $Lease['Tenant']['full_name'] ,
                                     array('controller'=>'tenants', 'action' => 'view', $Lease['Tenant']['id'])
                                     ); ?>

			&nbsp;
		</dd>
		<dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo h($Lease['Unit']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lease Type'); ?></dt>
		<dd>
			<?php echo h($Lease['LeaseType']['display_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Beginning of Lease'); ?></dt>
		<dd>
            <?php
            echo $this->Time->format($Lease["Lease"]["date_to"], "%Y-%m-%d"); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End of Lease'); ?></dt>
		<dd>
            <?php
            echo $this->Time->format($Lease["Lease"]["date_from"], "%Y-%m-%d"); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo h($property["Property"]["name"]); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount due per period'); ?></dt>
		<dd>
			<?php echo h( $this->Number->currency( $Lease['Lease']['due']) ); ?>
			&nbsp;
		</dd>
	</dl>
		<?php echo $this->Html->link(__('Edit  Lease'), 
                                     array('action' => 'edit', $Lease['Lease']['id']), 
                                     array( 'class' => 'btn-small btn-info' ) ); ?>
		<?php echo $this->Form->postLink(__('Delete  Lease'), 
                                         array('action' => 'delete', $Lease['Lease']['id']), 
                                         array( 'class' => 'btn-small btn-danger' ),
                                          __('Are you sure you want to delete # %s?', $Lease['Lease']['id'])); ?>
</div>
