<?
    $this->Html->addCrumb('Leases', '/leases');
    $this->Html->addCrumb('Lease Add', '');
?>
<script type="text/javascript">

$(function() {
    $( ".datepick" ).datepicker( {  dateFormat: "yy-mm-dd" });
});

</script>
<div class="row-fluid">
    <div class="span12">
        <?php echo $this->Form->create('Lease');?>
        <fieldset>
            <legend> <i class="icon-file-text-alt icon-2x"></i> <?php echo __('Add  Lease'); ?></legend>

        <?php
            $default = array();
            if (isset($selected_id))
                $default = array('default' => $selected_id);

            echo $this->Form->input('tenant_id',$default);
            echo $this->Form->input('unit_id');
            echo $this->Form->input('lease_type_id' );
            echo $this->Form->input('date_from', array('type'=>'text', 'class'=>'datepick') );
            echo $this->Form->input('date_to', array('type'=>'text', 'class'=>'datepick') );
            echo $this->Form->input('due', array('label'=>'Amount due per period','type'=>'text' ) );
            echo $this->Form->input('deposit', array('label'=>'Deposit','type'=>'text' ) );
        ?>

        </fieldset>
        <?php echo $this->Form->end( array( 'label' => 'Create Lease', 'class' => 'btn-small btn-success') );?>
    </div>
</div>
