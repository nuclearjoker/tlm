<?
$cakeDescription = __d('cake_dev', 'TLM');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<?php
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');	
	
	echo $this->Html->css('bargraph.css');
	echo $this->Html->css('cake.generic.mod.css');
        
	echo $this->Html->css('bootstrap.css');
	
	//<meta name="viewport" content="width=device-width, initial-scale=1.0">
	echo $this->Html->css('bootstrap-responsive.css');
	echo $this->Html->css('flat-ui.css');
	echo $this->Html->css('font-awesome.min.css');
	
	echo $this->Html->css('jquery-ui-1.10.3.custom.min.css');
	
	echo $this->fetch('css');
	
        echo $this->Html->script('jquery-1.7.2.min.js');
	echo $this->Html->script('jquery-ui-1.10.0.custom.min.js');
        echo $this->Html->script('jquery.easing.1.3.js');
        echo $this->Html->script('bootstrap.js');
        echo $this->fetch('script');

	?>
      <style type="text/css">
	  /* Sticky footer styles
	  -------------------------------------------------- */

	  html,
	  body {
	    height: 100%;
        background-image: url("<?  echo $this->webroot."/img/brickwall_@2X.png" ; ?>"); 
        background-position: initial initial;
        background-repeat: initial initial; 
	    /* The html and body elements cannot have any padding or margin. */
	  }

          a {
              text-decoration:none;
          }
	  /* Wrapper for page content to push down footer */
	  #wrap {
            min-height: 100%;
	    height: auto !important;
	    height: 100%;
	    /* Negative indent footer by it's height */
	    margin: 0 auto -20px;
	  }
    
	  /* Set the fixed height of the footer here */
	  #push,

	  #footer {
            backgroundcolor: transparent;
	    height: 20px;
	  }
          .trans {
              background-color: transparent;
            }
            /*For Pagination: */
            .pagination .current,
            .pagination .disabled {
                float: left;
                padding: 0 14px;

                color: #999;
                cursor: default;
                line-height: 34px;
                text-decoration: none;

                border: 1px solid #DDD;
                border-left-width: 0;
            }
      </style>
</head>
<body>

<div id="wrap">


  <!-- <div class="row-fluid">
    <div class="span12">
	    <div id="header">
	    
	    <h4>
	      <?php echo __('The Letting Manager')  ?>
	    </h4>
	    
	</div>
  </div> -->
<div class="navbar navbar-inverse" role="navigation">
  <div class="navbar-inner">
    <div class="container">
<?/* echo $this->Html->link( 
                          $this->Html->image('home.smallest.png', array('alt' => '(Logo)','class' => 'brand')),
                          '/',  array('escape' => false, 'class' => '') 
                        ); */
?>
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="nav-collapse">
        <ul class="nav">
          <li>  
               <? echo $this->Html->link('Properties', '/properties' ); ?> 		
              <ul>
                <li> <? echo $this->Html->link('Units', '/units' ); ?></li>
              </ul>
          </li> 
          <li>
             <? echo $this->Html->link('Tenants', '/tenants' ); ?>	
              <ul>
                <li><? echo $this->Html->link('Monthly Costs', '/monthly_costs' ); ?></li>	
                <li><? echo $this->Html->link('Utilities', '/utility_types' ); ?>	 <li>
              </ul>
          </li>
          <li>
              <? echo $this->Html->link('Meters', '/meters' ); ?> 
               <!-- <ul>	
                 <li> <? echo $this->Html->link('Readings', '/readings' ); ?> </li> 
                  <li> <? echo $this->Html->link('Meter Types', '/meter_types' ); ?> </li>
              </ul> -->
          </li>
          <li>
            <a href="#">Misc</a>
            <ul>
             <li> <? echo $this->Html->link('Users', '/users' ); ?> </li>
             <li> <? echo $this->Html->link('Groups', '/groups' ); ?> </li>
            </ul>
          </li>
          
         <li class="pull-right">
            <?php if (AuthComponent::user('id')): ?>
                <?  echo $this->Html->link('Logout', '/users/logout' ); ?>
            <?php endif; ?>
          </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>



<div class="container">
  <div class="row-fluid">
    <div class="span12">
			  <?php echo $this->Session->flash(); ?>
      <div class="row-fluid trans">
          <div class="span12 trans"> 
               <? echo $this->Html->getCrumbList(array( 'class' => 'breadcrumb', 
                                                        'separator' => ' > ',
                                                        'lastClass' => 'active'
                                                       ),
                                                 array( 'Home' => '/')
                                                ); ?>
          </div>
       </div>

			  
    </div>
   
  </div>
        <?php echo $this->fetch('content'); ?>
  </div>
  
</div>
    
  <?php  echo $this->element('sql_dump'); ?>
  <!-- Js writeBuffer -->
  <?php
    if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
    // Writes cached scripts
    ?>
    
  </div><!-- end of wrap -->

  <div id="footer" class="hidden-phone trans">
        <div >
            <p style="text-align:center">
              The Letting Manger &copy; <? echo date('Y'); ?> 
              <!-- <? echo Configure::version(); ?> -->
            </p>
        </div>
 </div>
  

</body>
</html>
