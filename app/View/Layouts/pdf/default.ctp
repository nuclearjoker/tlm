<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php /*
		echo $this->Html->css('cake.generic.mod.css',array('fullBase'=> true));
		echo $this->Html->css('flatui.css',array('fullBase'=> true));
		echo $this->Html->css('bootstrap.css',array('fullBase'=> true));
                echo $this->Html->css('font-awesome.min.css',array('fullBase'=> true));
		 */
	?>
</head>
<style>
.righty {
    text-align: right;
}

body {
color: #34495e;
font: 14px/1.231 "Lato", sans-serif;
}

.account {
  border-style: solid;
  border-width: 1px;
}
  
.table {
width: 100%;
margin-bottom: 20px;
}
table {
max-width: 100%;
background-color: transparent;
border-collapse: collapse;
border-spacing: 0;
}

.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
background-color: #f5f5f5;
}
.table th {
font-weight: bold;
}

.table th, .table td {
padding: 8px;
line-height: 20px;
text-align: left;
vertical-align: top;
border-top: 1px solid #dddddd;
}

tbody {
display: table-row-group;
vertical-align: middle;
border-color: inherit;
}

h1 {
font-size: 32px;
font-weight: 900;
}
h2 {
font-size: 26px;
font-weight: 700;
margin-bottom: 2px;
}
h3 {
font-size: 24px;
font-weight: 700;
margin-bottom: 4px;
margin-top: 2px;
}

h1, h2, h3 {
line-height: 40px;
}
h1, h2, h3, h4, h5, h6 {
margin: 10px 0;
font-family: inherit;
font-weight: bold;
line-height: 20px;
color: inherit;
text-rendering: optimizelegibility;
}

</style>
<body>
	<div id="container">

			<?php echo $this->fetch('content'); ?>
		  <br>
	</div>
</body>
</html>
