<?
    $this->Html->addCrumb('Properties', '/properties');
    $this->Html->addCrumb('Units', '/units');
    $this->Html->addCrumb('Unit View', '');
?>
<div class="span12">
<h2><?php  echo __('Unit: '); echo h($unit['Unit']['name']); ?></h2>
	<dl>
		<dt><?php echo __('name'); ?></dt>
		<dd>
			<?php echo h($unit['Unit']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo $this->Html->link($unit['Property']['name'], array('controller' => 'properties', 'action' => 'view', $unit['Property']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
<ul>
    <li><?php echo $this->Html->link(__('Edit Unit'), array('action' => 'edit', $unit['Unit']['id'])); ?> </li>
    <li><?php echo $this->Form->postLink(__('Delete Unit'), array('action' => 'delete', $unit['Unit']['id']), null, __('Are you sure you want to delete # %s?', $unit['Unit']['id'])); ?> </li>
    <br>
</ul>
</div>
