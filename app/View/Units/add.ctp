<?
    $this->Html->addCrumb('Units', '/units');
    $this->Html->addCrumb('Add Unit', '');
?>

<div class="row-fluid">
    <div class="span12">
    <?php echo $this->Form->create('Unit');?>
        <fieldset>
            <legend><?php echo __('Add Unit'); ?></legend>
        <?php
            echo $this->Form->input('name',array('type'=>'text'));
            $default = array();
            if (isset($selected_id))
                $default = array('default' => $selected_id);

            echo $this->Form->input('property_id',$default);
        ?>
        </fieldset>
    <?php echo $this->Form->end(__('Submit'));?>
    </div>
</div>
