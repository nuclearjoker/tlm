<?
    $this->Html->addCrumb('Properties', '/properties');
    $this->Html->addCrumb('Units', '');
?>
<div class="fluent-row ">
  <div class="span12 well">
	<h2><?php echo __('Units');?></h2>
	<div class="bnt-toolbar">
		<?php echo $this->Html->link(__('New Unit'), array('action' => 'add'), 
                                    array('class' => "btn-small btn-success" ) ); ?>
	</div>
	<table class="table-hover table-condensed">
	<tr>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('property_id');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($units as $unit): ?>
	<tr>
		<td><?php echo $this->Html->link($unit['Unit']['name'], array('action' => 'view', $unit['Unit']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($unit['Property']['name'], array('controller' => 'properties', 'action' => 'view', $unit['Property']['id'])); ?>
		</td>
		<td >
            <div class="btn-group">
			<?php echo $this->Html->link(__('<i class="icon-search"></i>View'), array('action' => 'view', $unit['Unit']['id']), 
                                         array( 'class' => 'btn btn-small', 'escape'=>false )); ?>
			<?php echo $this->Html->link(__('<i class="icon-edit"></i>Edit'), array('action' => 'edit', $unit['Unit']['id']), 
                                         array( 'class' => 'btn btn-small', 'escape'=>false )); ?>
			<?php echo $this->Form->postLink(__('<i class="icon-remove"></i>Delete'), array('action' => 'delete', 
                                             $unit['Unit']['id']), array( 'class' => 'btn btn-small', 'escape'=>false ),
                                             array('Are you sure you want to delete # %s?', $unit['Unit']['name'])); ?>
            </div>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
     <div class="pagination">  
        <? echo $this->Paginator->numbers(array(
            'before' => '<div class="pagination"><ul>',
            'separator' => '',
            'currentClass' => 'active',
            'tag' => 'li',
            'after' => '</ul></div>'
        ));
        ?>
      </div>
</div>
