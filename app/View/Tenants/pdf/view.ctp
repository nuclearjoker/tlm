

 <h1><i class="icon-building"></i>Account for <?php echo date('F Y', strtotime($selected_month)); ?></h1>
	<br>
	<br>
<div>
<table class="table">
    <tr>
            <td><?php echo __('Name:'); ?></td>
            <td>
                    <?php echo h($tenant['Tenant']['name']); ?>
                    &nbsp;
            </td>
    </tr>
    <tr>
            <td class="righty"><?php echo __('Surname:'); ?></td>
            <td>
                    <?php echo h($tenant['Tenant']['surname']); ?>
                    &nbsp;
            </td>

    </tr>
    <tr>
            <td class="righty"><?php echo __('Email:'); ?></td>
            <td>
                    <?php echo str_replace(",",",<br>",$tenant['Tenant']['email']); ?>
                    &nbsp;
            </td>
    </tr>
    <tr>
            <td class="righty"><?php echo __('Phone:'); ?></td>
            <td>
                    <?php echo h($tenant['Tenant']['phone']); ?>
                    &nbsp;
            </td>
    </tr>
    <tr>
            <td class="righty"><?php echo __('Lease:'); ?></td>
            <td>
                     <?php 
                        foreach( $leases as $lease) {
                            echo  h($lease["Unit"]["name"]); 
                        }
                    ?>
            </td>
    </tr>
</table>
</div>
<br>
<h4>Account Breakdown</h4>
<br>
 <table class="table table-hover" >
        <tr><th>Detail</th><th>Units</th><th>Cost Per Unit</th><th>Cost</th></tr>
        <?php
         foreach( $costs as $acost) {
            if( !empty($acost["link"]) ) {
                echo "<tr><td>";
                echo  h($acost["detail"]);
                echo "</td>";
            } else {
                echo "<tr><td>".$acost["detail"]."</td>";
            }
            echo "<td>".$acost["units"]."</td>";
            echo "<td>".$this->Number->currency($acost["cost_per_unit"])."</td>";
            echo "<td class='pull-right'>".$this->Number->currency($acost["cost"])."</td></tr>";
        }
        echo "<tr><td></td><td></td><td ><b>Total:</b></td><td class='pull-right'> ".$this->Number->currency($totalcost)."</td></tr>";
        ?>
</table>
