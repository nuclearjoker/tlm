<?
    $this->Html->addCrumb('Tenants', '');
?>
  
<div class="row-fluid well">
  <div class="span12">
   <div class="row-fluid "> <h1 class="tile-hot">
        <?php echo $this->Html->link(__('New Tenant'), array('action' => 'add'), array( 'class' => 'btn-small btn-success pull-right' )); ?>
       <i class="icon-user"></i> <?php echo __('Tenants');?></h1>
   </div>
      <table class="table table-hover table-striped table-condensed">
	      <tr>
			      <th><?php echo $this->Paginator->sort('name');?></th>
			      <th class="hidden-phone"> <?php echo $this->Paginator->sort('surname');?></th>
			      <th class="hidden-phone hidden-tablet"><?php echo 'Current Lease'; ?></th>
			      <th><?php echo "Balance"; ?> </th>
			      <th class="hidden-phone" ><?php echo "Last Payment"; ?> </th>
			      <th><?php echo __('');?> </th>
	      </tr>
	
	<?php
	  foreach ($tenants as $tenant): 
		$rowclass = "";
		if($tenant['Tenant']['balance'] <= -0.49 ) $rowclass = "error"; 
		if(empty($tenant['Tenant']['balance'])) $rowclass = "";     ?>
	      <tr  class="<? echo $rowclass; ?>">
		<td>
			<?php echo $this->Html->link($tenant['Tenant']['name'], array('action' => 'view', $tenant['Tenant']['id']));  ?>&nbsp;
		</td>
		<td class="hidden-phone">
			<?php echo h($tenant['Tenant']['surname']); ?>&nbsp;
		</td>
	        <td class="hidden-phone hidden-tablet">
			<?php 
                if( !empty($leases[ $tenant['Tenant']['id'] ]) )
                    echo $leases[ $tenant['Tenant']['id']  ]['Unit']['name'];                
                else
                    echo "N/A";
            ?>
		</td>
		<td>
			<?php echo h(round($tenant['Tenant']['balance'], 2) ); ?>&nbsp;
		</td>
		<td  class="hidden-phone">
			<?php if ( $last_payments[ $tenant['Tenant']['id'] ] != null ) {
				$r = split(' ',$last_payments[ $tenant['Tenant']['id'] ]);
                                echo $r[0];
                               } else
				echo "N/A";?>&nbsp;</td> 
		</td>
		<td>
            <div class="btn-group">
			<?php echo $this->Html->link( __('View'), array('action' => 'view', $tenant['Tenant']['id']), array("class" => "btn btn-small" ) ); ?>
			<?php echo $this->Html->link( __('Edit'), array('action' => 'edit', $tenant['Tenant']['id']),  array("class" => "btn btn-small" )  ); ?>
			<?php echo $this->Form->postLink( __('Delete'), array('action' => 'delete', $tenant['Tenant']['id']), array("class" => "btn btn-small" ), __('Are you sure you want to delete # %s?', $tenant['Tenant']['id'])); ?>
            </div>
		</td>
	     </tr>
	<?php endforeach; ?>
	</table>

<?php $span = isset($span) ? $span : 8; ?>
<?php $page = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1; ?>

      <div class="pagination">
      </div>
      
<!--
<div class="pagination pagination-small pagination-right">
    <ul>
        <?php echo $this->Paginator->prev(
            '&larr; ' . __('Previous'),
            array(
                'escape' => false,
                'tag' => 'li'
            ),
            '<a onclick="return false;">&larr; Previous</a>',
            array(
                'class'=>'disabled prev',
                'escape' => false,
                'tag' => 'li'
            )
        );?>
        
        <?php $count = $page + $span; ?>
        <?php $i = $page - $span; ?>
        <?php while ($i < $count): ?>
            <?php $options = ''; ?>
            <?php if ($i == $page): ?>
                <?php $options = ' class="active"'; ?>
            <?php endif; ?>
            <?php if ($this->Paginator->hasPage($i) && $i > 0): ?>
                <li<?php echo $options; ?>><?php echo $this->Html->link($i, array("page" => $i)); ?></li>
            <?php endif; ?>
            <?php $i += 1; ?>
        <?php endwhile; ?>
        
        <?php echo $this->Paginator->next(
            __('Next') . ' &rarr;',
            array(
                'escape' => false,
                'tag' => 'li'
            ),
            '<a onclick="return false;">Next &rarr;</a>',
            array(
                'class' => 'disabled next',
                'escape' => false,
                'tag' => 'li'
            )
        );?>
    </ul>
</div> -->

   </div>
</div>



