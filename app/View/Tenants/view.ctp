<?
    //debug($leases);
    $this->Html->addCrumb('Tenants', '/tenants');
    $this->Html->addCrumb('Tenant View', '');
    

?>
<?php
    
	//this checks wether deductions have been made this month.

?>


<script> 
  $(document).ready( function() {
         urlstr =  "<? echo $this->Html->url( array('controller'=>'tenants','action' => 'view', $tenant['Tenant']['id']),true); ?>";
	$('.datefield').datepicker();
        $('.datefield').datepicker('option','dateFormat','yy-mm-dd');
        $('.datefield').datepicker("setDate", new Date());
        $('#month').change( function(){
          window.console.log ( urlstr+"/"+$(this).find("option:selected").attr('value') );
          window.location.href = urlstr+"/"+$(this).find("option:selected").attr('value'); 
        });
        
        
        boxes = $('.well');
        maxHeight = Math.max.apply(
          Math, boxes.map(function() {
          return $(this).height();
       }).get());
       boxes.height(maxHeight+30);
       
       //for each element that is classed as 'pull-down', set its margin-top to the difference between its own height and the height of its parent
      $('.pull-down').each(function() {
          $(this).css('margin-top', $(this).parent().height()-$(this).height())
      });

  });
</script>


<h1 class="page-header"> <? echo h($tenant['Tenant']['name'])." ".h($tenant['Tenant']['surname']); ?> </h1>


<div class="row-fluid">
  <div class="span4 well">
  
            <div class="row-fluid">
             
	     <div class="btn-group select pull-right">

		<button class="btn dropdown-toggle clearfix btn-primary" data-toggle="dropdown"><span class="filter-option pull-left">Actions</span>&nbsp;<span class="caret"></span></button>
		<ul class="dropdown-menu">
		  <li><?php echo $this->Html->link(__('List Transactions'), array('controller' => 'transactions', 'action' => 'index', $tenant['Tenant']['id'] ), array('class' => '')); ?> </li>
		
		  
		  <li><?php 
                            if (!$deductionsmade) {
				echo $this->Html->link(__('Add Transactions'), 
							  array('controller' => 'tenants', 'action' => 'add_transactions', 
							$tenant['Tenant']['id'], $selected_month), null, 
							__('Are you sure you want to deduct the account from the balance for %s?', $tenant['Tenant']['name'])); 
                            }
                     ?>
		  </li>
		  <li>
		      <?php echo $this->Html->link(__('Edit Tenant'), array('action' => 'edit', $tenant['Tenant']['id']), array('class' => '')); ?> 
		  </li>
		  <li><?php echo $this->Form->postLink(__('Delete Tenant'),
						        array('action' => 'delete', $tenant['Tenant']['id']), null , 
							__('Are you sure you want to delete %s %s?', $tenant['Tenant']['name'], $tenant['Tenant']['surname'])); ?> 
		  </li>
	    </div>
            <h3><?php echo __('Details');?></h3>
            </div>
 
      
	    <div class="container">
		<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($tenant['Tenant']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Surname'); ?></dt>
		<dd>
			<?php echo h($tenant['Tenant']['surname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo str_replace(",",",<br>",$tenant['Tenant']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($tenant['Tenant']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Curent Lease '); ?>
                </dt>
		<dd>
                        <?php 
                            foreach( $leases as $lease) {
                                //debug($lease);
                                echo $this->Html->link( h($lease["Unit"]["name"]." Lease"), 
                                                        array('controller'=>'leases','action' => 'view',$lease["Lease"]['id']))."<br>"; 
                            }
                        ?>
                   
		  	<?php echo $this->Html->link(__('List Leases'), 
                                    array('controller' => 'leases', 'action' => 'index', $tenant['Tenant']['id'] ), 
				    array('class' => 'btn-small btn-inverse')); ?> <br>
		  	<?php echo $this->Html->link(__('Add Lease'), 
                                    array('controller' => 'leases', 'action' => 'add', $tenant['Tenant']['id'] ), 
				    array('class' => 'btn-small btn-success')); ?> <br>
		</dd>
		<dt><?php echo __('Outstanding Balance'); ?></dt>
		<dd>
			<?php echo $this->Number->currency((-1*$tenant['Tenant']['balance'])); ?>
			&nbsp;
		</dd> <br>
		<dt><?php echo __('Add Payment:'); ?></dt>
		<dd>
                 <form class="form" action="<?php echo $this->Html->url(array('controller'=>'transactions','action'=>'add_payment',$tenant['Tenant']['id'])); ?>" id="TransactionAddForm" method="post" accept-charset="utf-8">
                  <input type="hidden" name="_method" value="POST"/>
                  <label> Amount: </label>
                  <input name="data[Transaction][cost]" length="6" type="text" />
                  <label> Date:  </label>
                  <input class="datefield" name="data[Transaction][transact_date]" length="6" type="text" value="<? echo date('Y-m-d'); ?>" /><br>
                  <input  type="submit" value="Add Payment" class="btn-small btn-success" />
                 </form>
                </dd>
                </dl>
              </div>
    </div>
    
    <div class="span8 pull-left  well">
        <div class="form-inline pull-right">
            <?
            echo $this->Form->input('month', array(
                    'options' => $listed_dates, 
                    'selected' => $selected_month
                ));            
            ?>
        </div>    
        <?php 
                $acc_type = "Account";
                if($deductionsmade)
                    $acc_type = "Transactions";
        ?>
	<h3 > <? echo $acc_type; ?> for <?php echo date('F Y', strtotime($selected_month)); ?></h3><br>
        
        <div class="row-fluid">
            <div class="span12">
	<div class="btn-group">
	    
	      <?php echo $this->Html->link(__('View Account PDF'), 
					    array('action' => 'view', $tenant['Tenant']['id'], $selected_month, 'ext' => 'pdf'), 
					    array('class' => 'btn-small btn-info') ); ?>  
	      <?php echo $this->Html->link(__('New Monthly Cost'), array('controller' => 'monthly_costs', 'action' => 'add', $tenant['Tenant']['id']   ), 
					    array('class' => 'btn-small btn-success') );?> 
              <?php echo $this->Html->link(__('Email Account'), array('controller' => 'monthly_costs', 'action' => 'email', $tenant['Tenant']['id']   ), 
					    array('class' => 'btn-small btn-warning') );?> 
	</div>
        </div>
        </div>
        <table class="table table-hover table-striped table-condensed" >
		<tr><th>Detail</th><th>Units</th><th>Cost Per Unit</th><th>Cost</th></tr>
		<?php
                // THINK ABOUT THIS STILL: BBF: echo "<tr><td> Balance Brought Forward </td><td></td><td></td><td style='text-align:right'> R ".outR(-1*$tenant['Tenant']['balance'])."</td></tr>";
                
                foreach( $costs as $acost) {
                    if( !empty($acost["link"]) ) {
                        echo "<tr><td>";
                        echo  $this->Html->link( h($acost["detail"]), $acost["link"] );
                        echo "</td>";
                    } else {
                        echo "<tr><td>".$acost["detail"]."</td>";
                    }
                    echo "<td>".$acost["units"]."</td>";
                    echo "<td><p class='pull-right'>".$this->Number->currency($acost["cost_per_unit"])."</p></td>";
                    echo "<td ><p class='pull-right'>".$this->Number->currency($acost["cost"])."</p></td></tr>";
                }
                echo "<tr><td></td><td></td><td ><b><p class='pull-right'>Total:</p></b></td><td><p class='pull-right'>".$this->Number->currency($totalcost)."</p></td></tr>";
                
		?>
	</table>
    </div>
</div>


