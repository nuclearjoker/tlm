<?                                                                                                               $this->Html->addCrumb('Tenants', '/tenants');
    $this->Html->addCrumb('Edit Tenant', '');
?>

<div class="row-fluid">
  <div class="span12">
	
      <legend><?php echo __('Edit Tenant'); ?></legend>
        <div class="btn-toolbar">
  	  <?php echo $this->Form->postLink(__('Delete Tenant'), 
				 array('action' => 'delete', $this->Form->value('Tenant.id')), 
                                 array( 'class' => 'btn-small btn-danger'), 
				 __('Are you sure you want to delete # %s?', 
			    	 $this->Form->value('Tenant.name')
				 )
				); 
		?>
	 </div>
       <?php echo $this->Form->create('Tenant');?>
	<fieldset>
	<?php
		echo $this->Form->input('name',array('type'=>'text'));
		echo $this->Form->input('surname',array('type'=>'text'));
		echo $this->Form->input('email',array('type'=>'text'));
		echo $this->Form->input('phone',array('type'=>'text'));
		echo $this->Form->input('balance',array('type'=>'text'));
	?>
	</fieldset>
      <?php echo $this->Form->end( array( 'label' => 'Submit Changes', 'class' => 'btn-small btn-success') );?>
  </div>
</div>
