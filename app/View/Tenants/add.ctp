<?                                                                                                             
    $this->Html->addCrumb('Tenants', '/tenants/index');
    $this->Html->addCrumb('Add Tenants', '');
?>

<div class="tenants form">
<?php echo $this->Form->create('Tenant');?>
	<fieldset>
		<h2><?php echo __('Add Tenant'); ?></h2>
    <div class="btn-toolbar">
            <?php echo $this->Html->link(__('List other tenants'),  array('action' => 'index'),
                                                                    array("class"=>"btn-small btn-info")); ?>
    </div>
	<?php
		echo $this->Form->input('name',array('type'=>'text'));
		echo $this->Form->input('surname',array('type'=>'text'));
		echo $this->Form->input('email',array('type'=>'email'));
		echo $this->Form->input('phone',array('type'=>'tel'));
		echo $this->Form->input('idnumber',array('type'=>'tel'));
	?>
	</fieldset>
    <?php echo $this->Form->end( array( 'label' => 'Add Tenants', 
                                    "class" => "btn btn-success") );
    ?>
</div>
