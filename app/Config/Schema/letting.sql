CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE "utilites" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "tenant_id" INTEGER NOT NULL DEFAULT (0),
    "utility_id" INTEGER NOT NULL DEFAULT (0),
    "units" REAL NOT NULL DEFAULT (0)
);
CREATE TABLE "houses" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "name" TEXT NOT NULL
);
CREATE TABLE "units" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "Name" TEXT NOT NULL,
    "house_id" INTEGER NOT NULL
);
CREATE TABLE tenants (
    "id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "surname" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "phone" TEXT NOT NULL,
    "unit_id" INTEGER NOT NULL DEFAULT (0)
);
CREATE TABLE "utility_types" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "name" TEXT,
    "costperunit" REAL NOT NULL DEFAULT (0)
);
CREATE TABLE "bill" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "deleted" INTEGER NOT NULL DEFAULT (0),
    "period_to" TEXT NOT NULL,
    "period_from" TEXT NOT NULL,
    "file" BLOB
);
