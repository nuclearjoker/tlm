<?php
include("parse.php");
App::uses('AppController', 'Controller','Reading','Property');

class MetersController extends AppController {
  public $helpers = array('Js' => array('Jquery'));
  public $components = array('Filter.Filter');
  public $paginate = array(
          'limit' => 15);


   var $filters = array
   (
      'index' => array
        (
          'Meter' => array
          (
            'Meter.name',
            'Meter.property_id' => array
            (
                  'type' => 'select',
                  'label' => 'Property',
                  'selector' => 'getProperties'
            )
        )
      )
   );
    
    public function add($id=null) {
        Controller::loadModel('UtilityType');
        Controller::loadModel('Unit');
            if($id!=null)
                    $this->set('selected',$id);
            if ($this->request->is('post')) {
                    $this->Meter->create();
                    if ($this->Meter->save($this->request->data)) {
                            $this->Session->setFlash(__('The meter has been saved'));
                            $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Session->setFlash(__('The meter could not be saved. Please, try again.'));
                    }
            }

            $properties = $this->Meter->Property->find('list');
            $this->set(compact('properties'));

            $units = $this->Unit->find('list');
            $this->set(compact('units'));

            $utilityTypes = $this->UtilityType->find('list');
            $this->set(compact('utilityTypes'));
    }


    public function edit($id = null) {
        Controller::loadModel('UtilityType');
        Controller::loadModel('Unit');
            $this->Meter->id = $id;
            if (!$this->Meter->exists()) {
                    throw new NotFoundException(__('Invalid meter'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                    if ($this->Meter->save($this->request->data)) {
                            $this->Session->setFlash(__('The meter has been saved'));
                            $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Session->setFlash(__('The meter could not be saved. Please, try again.'));
                    }
            } else {
                    $this->request->data = $this->Meter->read(null, $id);
            }
            $properties = $this->Meter->Property->find('list');
            $this->set(compact('properties'));

            $units = $this->Unit->find('list');
            $this->set(compact('units'));

            $utilityTypes = $this->UtilityType->find('list');
            $this->set(compact('utilityTypes'));
    }


    public function index($propertyid=null) {
	$this->Meter->recursive = 1;
	$this->paginate = $this->Filter->paginate;

        $meters = $this->paginate();
	$this->set('Meters', $meters);


        // Get the last readings: 
	Controller::loadModel('Reading');

        $readings = array();
        $last_readings_date = array();
        $usage = array();
        
        $thismonth = date('Y-m');
        // Get readings:
        foreach( $meters as $meter) {
            $r = $this->Reading->getReading($meter['Meter']['id'], $thismonth);
            if(!empty($r) ) {
                $readings[$meter['Meter']['id']] = $r["units"];
            }
        }
        // Get usage for this month.
        foreach( $meters as $meter) {
            $r = $this->Reading->getUsage($meter['Meter']['id'], $thismonth);
            if(!empty($r) ) {
                $usage[$meter['Meter']['id']] = $r;
            }
        }
        // Get last readings:
        foreach( $meters as $meter) {
            $r = $this->Reading->getLastReading($meter['Meter']['id'], true);
            if(!empty($r) ) {
                $last_readings_date[$meter['Meter']['id']] = $r["for_month"];
                $last_readings[$meter['Meter']['id']] = $r["units"];
            }
        }

        
        $this->set('readings',$readings );
        $this->set('last_readings_date',$last_readings_date );
        $this->set('last_readings',$last_readings );
        $this->set('usage',$usage );
   }

	public function view($id = null) {
		$this->Meter->recursive = 2;
		$this->Meter->id = $id;
		if (!$this->Meter->exists()) {
			throw new NotFoundException(__('Invalid meter'));
		}
		$this->set('Meter', $this->Meter->read(null, $id));
//		$this->set('Readings', $this->Meter->find( 'all',array( 'conditions' => 
  //                                                                              array('limit' => 6) )) );
                Controller::loadModel('Reading');
                $readings =  $this->Reading->find( 'all', array( 'limit'=>7, 'order'=>'for_month ASC','conditions' => array( 'meter_id' => $id  ) ));
                $usage = array();           $average = 0;
                $daily_average = array();   $daily_average_average = 0;
                $i=0;
              
                if( count($readings) > 0) {
                    foreach($readings as $reading) {
                        $id = $reading['Reading']['id'];
                        $current_ts = strtotime($reading['Reading']['for_month']);
                        if($i>0) {
                            $current_usage = $reading['Reading']['units']-$last;
                            $usage[ $id ] = $current_usage;
                            
                            $no_days = floor(($current_ts-$last_ts)/86400);
                            $davg = round(( $current_usage/$no_days ), 2);
                            
                            $daily_average[ $id ] =  $davg;
                            //to calculate the total averages over n number of readings:
                            $average += $current_usage;
                            $daily_average_average +=  $davg;
                        }
                        $last = $reading['Reading']['units'];
                        $last_ts = $current_ts;
                        $last_id = $reading['Reading']['id'];
                        $i++;
                    }
		    if( $average > 0) {
	                    $average /= $i-1;
			}
                    if (count($readings) ==7 ){//if 7 where extracted, delete the first one.
                        unset( $readings[0] );
                    }else{ //the first element always has zero usage & daily average:
                        $usage[ $readings[0]["Reading"]["id"]] = 0;
                        $daily_average[ $readings[0]["Reading"]["id"]] = 0;
                    }                  
                }    
                
		$this->set('Readings', $readings );
                $this->set('daily_average', $daily_average );
                $this->set('daily_average_average', $daily_average_average );
                $this->set('average', $average);
                $this->set('usage', $usage);
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Meter->id = $id;
		if (!$this->Meter->exists()) {
			throw new NotFoundException(__('Invalid meter'));
		}
		if ($this->Meter->delete()) {
			$this->Session->setFlash(__(' meter deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__(' meter was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
