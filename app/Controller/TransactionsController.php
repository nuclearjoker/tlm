<?php
App::uses('AppController', 'Controller');
/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 */
class TransactionsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index( $id = null ) {
            $this->Transaction->recursive = 0;

            if ($id!=null) {
              $cond =  array('tenant_id'=>$id);
              $this->set('transactions', $this->paginate( null,$cond ));
            } else {
                       $this->set('transactions', $this->paginate());
            }
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Transaction->id = $id;
		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		$this->set('transaction', $this->Transaction->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Transaction->create();
			if ($this->Transaction->save($this->request->data)) {
				$this->Session->setFlash(__('The transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
			}
		}
		$tenants = $this->Transaction->Tenant->find('list');
		$this->set(compact('tenants'));
	}

	public function add_payment($tenantid=null) {
        Controller::loadModel('Tenant');

        if ($this->request->is('post')) {

            $balance = $this->Tenant->findById( $tenantid );
            $balance = $balance["Tenant"]["balance"];
            if($balance == null) $balance = 0;

            $this->Tenant->id  = $tenantid;
            $this->Tenant->saveField( 'balance', $balance +  $this->request->data["Transaction"]["cost"] );

            $atrans["detail"] = "Payment received.";
            $atrans["cost_per_unit"] = 1;
            $atrans["units"] = 1;
            $atrans["date"] =  $this->request->data["Transaction"]["transact_date"];
            $atrans["cost"] = $this->request->data["Transaction"]["cost"];
            $atrans["tenant_id"] = $tenantid;
            $atrans["transaction_type"] = +1;

            $transaction["Transaction"] = $atrans;


            if ($this->Transaction->save($transaction)) {
                         $this->Session->setFlash(__('The payment has been saved'));
                         //$this->redirect(array('action' => 'index'));
                 } else {
                         $this->Session->setFlash(__('The payment could not be saved. Please, try again.'));
                 }
            }
		
	    $this->redirect(array('controller'=>'tenants','action' => 'view',$tenantid));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Transaction->id = $id;
		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Transaction->save($this->request->data)) {
				$this->Session->setFlash(__('The transaction has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Transaction->read(null, $id);
		}
		$tenants = $this->Transaction->Tenant->find('list');
		$this->set(compact('tenants'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Transaction->id = $id;
		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		if ($this->Transaction->delete()) {
			$this->Session->setFlash(__('Transaction deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Transaction was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
