<?php
App::uses('AppController', 'Controller');
/**
 * Leases Controller
 *
 * @property Lease $Lease
 */
class LeasesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index($tenantid=null) {
        Controller::loadModel('Tenant');
        Controller::loadModel('LeaseType');
        if( $tenantid!=null) {
            $this->paginate = array(
             'conditions' => array('Lease.tenant_id' => $tenantid)
            );
            $this->set('Tenant', $this->Tenant->findById($tenantid) );
        } 
        $this->Lease->recursive = 1;
        $this->set('leases', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        Controller::loadModel('Property');
		$this->Lease->id = $id;
		if (!$this->Lease->exists()) {
			throw new NotFoundException(__('Invalid lease type'));
		}
        $lease = $this->Lease->read(null, $id);
		$this->set('Lease', $lease);

        $this->Property->recursive = 0;
        $this->set('property', $this->Property->findById($lease["Unit"]["property_id"]) );
	}

/**
 * add method
 *
 * @return void
 */
	public function add($tenantid=null) {
            Controller::loadModel('Tenant');
            Controller::loadModel('Unit');
            Controller::loadModel('LeaseType');

            if($tenantid != null)
                $this->set('selected_id',$tenantid);

                    if ($this->request->is('post')) {
                            $this->Lease->create();
                            if ($this->Lease->save($this->request->data)) {
                                    $this->Session->setFlash(__('The lease has been saved'));
                                    $this->redirect(array('action' => 'index'));
                            } else {
                                    $this->Session->setFlash(__('The lease could not be saved. Please, try again.'));
                            }
                    }

            $leasetypes = $this->LeaseType->find('list');
            $this->set('leaseTypes', $leasetypes );

            $this->set('tenants', $this->Tenant->getTenants() );
            $this->set('units', $this->Unit->find('list'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        Controller::loadModel('Tenant');
        Controller::loadModel('Unit');
        Controller::loadModel('LeaseType');

		$this->Lease->id = $id;
		if (!$this->Lease->exists()) {
			throw new NotFoundException(__('Invalid lease'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Lease->save($this->request->data)) {
				$this->Session->setFlash(__('The lease has been saved'));
				if ( $id != null)
					$this->redirect(array('action' => 'view', $id));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lease could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Lease->read(null, $id);
		}

        $this->Tenant->recursive = -1;
        $tenants = $this->Tenant->find('all');

        $tenantsarr = array();
        foreach( $tenants as $tenant) {
            $viewstr = $tenant["Tenant"]["name"]." ".$tenant["Tenant"]["surname"];
            if ( !empty($tenant["Tenant"]["id_number"]) ) {
                $viewstr .= " (".$tenant["Tenant"]["id_number"].")";
            }
            $tenantsarr[ $tenant["Tenant"]["id"] ] = $viewstr;
        }

        $leasetypes = $this->LeaseType->find('list');
        $this->set('leaseTypes', $leasetypes );

        $this->set('tenants',$tenantsarr);
        $this->set('units', $this->Unit->find('list'));

	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Lease->id = $id;
		if (!$this->Lease->exists()) {
			throw new NotFoundException(__('Invalid lease'));
		}
		if ($this->Lease->delete()) {
			$this->Session->setFlash(__('Utility type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Utility type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
