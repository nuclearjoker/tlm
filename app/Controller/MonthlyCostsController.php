<?php
App::uses('AppController', 'Controller');
/**
 * MonthlyCosts Controller
 *
 * @property MonthlyCost $MonthlyCost
 */
class MonthlyCostsController extends AppController {
	
	public function index() {
		$this->MonthlyCost->recursive = 2;
		$this->set('monthlyCosts', $this->paginate());
	}

	public function view($id = null) {
		$this->MonthlyCost->id = $id;
		if (!$this->MonthlyCost->exists()) {
			throw new NotFoundException(__('Invalid monthly cost'));
		}
		$this->set('monthlyCost', $this->MonthlyCost->read(null, $id));
	}
        
        function convertDates( &$data ){
            if (!empty($data['date_from']) && strtotime($data['date_from']) ){
                $data['date_from'] = date('Y-m-d', strtotime($data['date_from']));
            }
            if (!empty($data['date_to']) && strtotime($data['date_to']) ){
                $data['date_to'] = date('Y-m-d', strtotime($data['date_to']));
            }
        }
        
	public function add($tenant_id = null) {
            Controller::loadModel('Tenant');
            Controller::loadModel('Meter');
            
		if ($this->request->is('post')) {
                        $this->convertDates( $this->request->data["MonthlyCost"]);
			$this->MonthlyCost->create();
			if ($this->MonthlyCost->save($this->request->data)) {
				$this->Session->setFlash(__('The monthly cost has been saved'));
                                if($tenant_id == null)
                                    $this->redirect(array('action' => 'index'));
                                else //once you add costs - take to the tenant view.
                                    $this->redirect(array('controller'=>'tenants','action' => 'view', $tenant_id)); 
			} else {
				$this->Session->setFlash(__('The monthly cost could not be saved. Please, try again.'));
			}
		}
                
                //Fetch utilities:
                $this->MonthlyCost->UtilityType->recursive = -1;
		$utilityTypes = $this->MonthlyCost->UtilityType->find('all');
                $r = array();
                foreach( $utilityTypes  as $ut) {
                    $r[ $ut['UtilityType']['id'] ] = $ut['UtilityType']['displayfield'].' ('.CakeNumber::currency($ut['UtilityType']['costperunit']).')';
                }
                $utilityTypes = $r;
                
                if ($tenant_id!=null) {
                    $this->Tenant->recursive = -1;
                    $tenant = $this->Tenant->findById($tenant_id);
                    $tenants[$tenant["Tenant"]["id"]] = $tenant["Tenant"]["name"]." ".$tenant["Tenant"]["surname"];
                } else {
                    $tenants = $this->Tenant->getTenants();
                }
                
                if ( $tenant_id == null)
                    $meters = $this->Meter->getMeters();
                else //$tenant has id of property at this point.
                    $meters =  $this->Tenant->getMeters($tenant_id, true);                
                                
       		$this->set(compact('meters', 'utilityTypes', 'tenants'));
	}

	public function edit($id = null, $tenant_id = null) {
                Controller::loadModel('Tenant');
                Controller::loadModel('Meter');
            
		$this->MonthlyCost->id = $id;
		if (!$this->MonthlyCost->exists()) {
			throw new NotFoundException(__('Invalid monthly cost'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
                        $this->convertDates( $this->request->data["MonthlyCost"]);
			if ($this->MonthlyCost->save($this->request->data)) {
				$this->Session->setFlash(__('The monthly cost has been saved'));
				if ( $tenant_id != null)
					$this->redirect(array('controller'=>'tenants','action' => 'view', $tenant_id));
				else
					$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The monthly cost could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->MonthlyCost->read(null, $id);
		}
                
                //Fetch utilities:
                $this->MonthlyCost->UtilityType->recursive = -1;
		$utilityTypes = $this->MonthlyCost->UtilityType->find('all');
                $r = array();
                foreach( $utilityTypes  as $ut) {
                    $r[ $ut['UtilityType']['id'] ] = $ut['UtilityType']['displayfield'].' ('.CakeNumber::currency($ut['UtilityType']['costperunit']).')';
                }
                $utilityTypes = $r;
                //Fetach Meters:
		if ( $tenant_id == null)    
                    $meters = $this->Meter->getMeters();
                else //$tenant has id of property at this point.
                    $meters =  $this->Tenant->getMeters($tenant_id, true);   
                
                //debug($meters);
                
		$utilityTypes = $this->MonthlyCost->UtilityType->find('list');
		$tenants = $this->MonthlyCost->Tenant->getTenants();
		$this->set(compact('meters', 'utilityTypes', 'tenants'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->MonthlyCost->id = $id;
		if (!$this->MonthlyCost->exists()) {
			throw new NotFoundException(__('Invalid monthly cost'));
		}
		if ($this->MonthlyCost->delete()) {
			$this->Session->setFlash(__('Monthly cost deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Monthly cost was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
