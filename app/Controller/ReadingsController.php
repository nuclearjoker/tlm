<?php

App::uses('AppController', 'Controller');

class ReadingsController extends AppController {

    public $uses = array("Reading", "Meter");
    public $components = array('Filter.Filter');

    public function index($meterid = null) {
        $this->Reading->recursive = 2;
        $cond = array();
        if ($meterid != null)
            $cond = array('Reading.meter_id' => $meterid);
        $this->set('meterid', $meterid);
        $this->set('Readings', $this->paginate(null, $cond));
    }

    public function view($id = null) {
        $this->Reading->id = $id;
        $this->Reading->recursive = 2;
        if (!$this->Reading->exists()) {
            throw new NotFoundException(__('Invalid  reading'));
        }
        $this->set('Reading', $this->Reading->read(null, $id));
    }

    public function add($meterid = null) {
        if ($this->request->is('post')) {
            $this->Reading->create();
                        
            if ($this->Reading->save($this->request->data)) {
                $this->Session->setFlash(__('The  reading has been saved'));
                $this->redirect(array('controller' => 'meters', 'action' => 'view', $meterid));
            } else {
                $this->Session->setFlash(__('The  reading could not be saved. Please, try again.'));
            }
        }
        $meters = $this->Reading->Meter->getMeters(true);
        
        $this->set(compact('meters'));
        if ($meterid != null)
            $this->set('selected_id', $meterid);
    }

    public function quick_add($meterid = null) {
        if ($this->request->is('post') && isset($meterid)) {
            $this->Reading->create();
            $this->request->data['Reading']["for_month"] = date('Y-m-d');
            $this->request->data['Reading']["date_taken"] = date('Y-m-d');
            $this->request->data['Reading']["meter_id"] = $meterid;
            //debug($this->request->data);
            if ($this->Reading->save($this->request->data)) {
                $this->Session->setFlash(__('The  reading has been saved'));
            } else {
                $this->Session->setFlash(__('The  reading could not be saved. Please, try again.'));
            }
        } else {
            $this->Session->setFlash(__('The  reading could not be saved. Please, try again.'));
        }
        $this->redirect(array('controller' => 'meters', 'action' => 'index'));
    }

    public function edit($id = null) {
        $this->Reading->id = $id;
        if (!$this->Reading->exists()) {
            throw new NotFoundException(__('Invalid  reading'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Reading->save($this->request->data)) {
                $this->Session->setFlash(__('The  reading has been saved'));
                //get meter id
                $d = $this->Reading->read(null, $id);
                $meter_id = $d['Reading']['meter_id'];
                $this->redirect(array('controller' => 'meters', 'action' => 'view', $meter_id));
            } else {
                $this->Session->setFlash(__('The  reading could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Reading->read(null, $id);
        }
        
        
        $Meters = $this->Reading->Meter->find('list');
        //debug($Meters);
        $this->set(compact('Meters'));
         
    }

    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Reading->id = $id;
        $d = $this->Reading->read(null, $id);
        $meter_id = $d['Reading']['meter_id'];
        if (!$this->Reading->exists()) {
            throw new NotFoundException(__('Invalid  reading'));
        }
        if ($this->Reading->delete()) {
            $this->Session->setFlash(__(' reading deleted'));
            $this->redirect(array('controller' => 'meters', 'action' => 'view', $meter_id));
        }
        $this->Session->setFlash(__(' reading was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
