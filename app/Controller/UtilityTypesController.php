<?php
App::uses('AppController', 'Controller');
/**
 * UtilityTypes Controller
 *
 * @property UtilityType $UtilityType
 */
class UtilityTypesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UtilityType->recursive = 0;
		$this->set('utilityTypes', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->UtilityType->id = $id;
		if (!$this->UtilityType->exists()) {
			throw new NotFoundException(__('Invalid utility type'));
		}
		$this->set('utilityType', $this->UtilityType->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UtilityType->create();
			if ($this->UtilityType->save($this->request->data)) {
				$this->Session->setFlash(__('The utility type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The utility type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->UtilityType->id = $id;
		if (!$this->UtilityType->exists()) {
			throw new NotFoundException(__('Invalid utility type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->UtilityType->save($this->request->data)) {
				$this->Session->setFlash(__('The utility type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The utility type could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->UtilityType->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->UtilityType->id = $id;
		if (!$this->UtilityType->exists()) {
			throw new NotFoundException(__('Invalid utility type'));
		}
		if ($this->UtilityType->delete()) {
			$this->Session->setFlash(__('Utility type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Utility type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
