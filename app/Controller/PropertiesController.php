<?php
App::uses('AppController', 'Controller','Tools.GoogleMapV3');
/**
 * Properties Controller
 *
 * @property Property $Property
 */
class PropertiesController extends AppController {
        public $paginate = array(
            'limit' => 10,
            'order' => array(
                'Property.name' => 'asc'
            )
        );
        
	public function index() {

		$this->Property->recursive = 0;
		$this->set('properties', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        $this->helpers[] = 'Tools.GoogleMapV3';
		$this->recursive = 2;
		$this->Property->id = $id;


		if (!$this->Property->exists()) {
			throw new NotFoundException(__('Invalid property'));
		}
    		Controller::loadModel('Meter');
		$meter = $this->Meter->find('all', array('conditions' => array( 'Meter.property_id' => $id),'recursive'=>0 ) );

		$property = $this->Property->read(null, $id);

		$property['Meter'] = $meter;
		$this->set('property', $property);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Property->create();
      //debug($this->request);
			if ($this->Property->save($this->request->data)) {

				$this->Session->setFlash(__('The property has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Property->id = $id;
		if (!$this->Property->exists()) {
			throw new NotFoundException(__('Invalid property'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Property->save($this->request->data)) {
				$this->Session->setFlash(__('The property has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Property->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Property->id = $id;
		if (!$this->Property->exists()) {
			throw new NotFoundException(__('Invalid property'));
		}
		if ($this->Property->delete()) {
			$this->Session->setFlash(__('Property deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Property was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
