<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class TenantsController extends AppController {
        public function index() {
		Controller::loadModel('Transaction');
                Controller::loadModel('Lease');
		$this->Tenant->recursive = 0;
		
		$last_payments = Array();
                $leases = Array();
		$this->Transaction->recursive = -1;
                
                $tenants = $this->paginate();
                
		foreach( $this->paginate() as $atenant) {
		  $tenid = $atenant["Tenant"]["id"];

		  $lastpayment = $this->Transaction->lastTransaction($tenid);

		  if ( !empty($lastpayment) )
		      $last_payments[$tenid] = $lastpayment[0]["Transaction"]["date"];
		  else
		      $last_payments[$tenid] = null;
                  
                  //Get the current lease for the tenant.
                  $lease = $this->Lease->getLeases($tenid);
                  if( !empty($lease) ) {
                            $leases[$tenid] = $lease[0];//always pick the first one.
                  }
		}
		$this->set('last_payments', $last_payments);
		$this->set('tenants', $tenants);
                $this->set('leases', $leases);
	}

        function getLeaseCosts($tenid = null,$selected_month=null){
            if ( $tenid == null || $selected_month == null)
               return array();
            Controller::loadModel('Lease');
            $leases = $this->Lease->getLeases($tenid,$selected_month);
            
            //add leases to costs array:
            $costs = array();
            $ts_selected = strtotime($selected_month);
            foreach( $leases as $lease ) {
                $acost = array(
	
                            "detail" => "Rent for ".date('M Y', strtotime("+1 month", $ts_selected))." (".$lease["Unit"]["Property"]["name"]." - ".$lease["Unit"]["name"].")",
                            "units" => 1, 
                            "cost_per_unit" => $lease["Lease"]["due"],
                            "cost" => $lease["Lease"]["due"],
                            "link" => array("controller"=>"leases", "action" => "view", $lease["Lease"]["id"])
                      );
                $costs[] = $acost;
            }
            return $costs;
        }
        
        function getMonthlyCosts($tenid=null,$selected_month=null) {
            if ( $tenid == null || $selected_month == null)
               return array();
            Controller::loadModel('MonthlyCost');
            Controller::loadModel('Reading');
            $sql_first_day = $selected_month."-1";  //first day of selected month, mysql format.
            $sql_last_day = $selected_month.'-'.date('t', strtotime($selected_month)); //last day of selected month, mysql format
            $ts_selected = strtotime($selected_month);
           
            //Get list of applicable monthly costs. (The monthly DATETIME is always stored as the first day, inclusive).
            //$this->MonthlyCost->recursive = -1;
            $monthly_costs = $this->MonthlyCost->find('all',
                     array( 
                         'conditions' => array( 'MonthlyCost.tenant_id' => $tenid,
                               'MonthlyCost.date_from <=' => $sql_first_day,
                               'MonthlyCost.date_to >=' =>  $sql_first_day,
                            ),
                          'contain' => array( 'UtilityType','Meter.UtilityType' ) 
                      
                   )
                 );

            $costs = array();
            //iterate and add to costs array:
            foreach( $monthly_costs as $mc ) {
                //unmetered mc:
                if( $mc["MonthlyCost"]["meter_id"] == null ) {
                    $acost = array(
                            "detail" => $mc["UtilityType"]["name"]." for ".date('M Y', $ts_selected),
                            "units" => $mc["MonthlyCost"]["units"], 
                            "cost_per_unit" => $mc["UtilityType"]["costperunit"],
                            "cost" => $mc["MonthlyCost"]["units"]*$mc["MonthlyCost"]["fraction_of_cost"]*$mc["UtilityType"]["costperunit"],
                            "link" => array("controller"=>"monthly_costs", "action" => "view", $mc["MonthlyCost"]["id"])
                     );
                } else {//metered:
                    $reading = $this->Reading->getUsage($mc["Meter"]["id"], $selected_month);
                    $acost = array(
                            "detail" => $mc["Meter"]["UtilityType"]["name"]." for ".date('M Y', $ts_selected),
                            "units" => $reading,
                            "cost_per_unit" => $mc["Meter"]["UtilityType"]["costperunit"],
                            "cost" => $reading*$mc["MonthlyCost"]["fraction_of_cost"]*$mc["Meter"]["UtilityType"]["costperunit"],
                            "link" => array("controller"=>"monthly_costs", "action" => "view", $mc["MonthlyCost"]["id"])
                     );   
                }
                $costs[] = $acost;
            }
            
            return $costs;
        }
        
	public function view($id = null,$selected_month=null) {
            Controller::loadModel('UtilityTypes');
            Controller::loadModel('Lease');
            Controller::loadModel('MonthlyCost');
            Controller::loadModel('Transaction');
            Controller::loadModel('Reading');
            Controller::loadModel('Transaction');
            
            if( $selected_month == null) {
                $selected_month = date('Y-m');
            } else {
                if (($timestamp = strtotime($selected_month)) === false) {
                    $this->Session->setFlash(__('Incorrect date format for the tenant was supplied.'));
                    $this->redirect(array('controller'=>'tenants', 'action' => 'view', $id));
                }
            }
            
            //selected month to show the account for.
            $this->set('selected_month',$selected_month);

            $this->Tenant->recursive = -1;
            $tenant = $this->Tenant->read(null, $id);
            $this->set('tenant', $tenant);
            
            //get the date of the last debit made. So we can only add transactions once per month.
            $last_debit = $this->Transaction->lastDebit($id,$selected_month);
            
            $deductionsmade = false;
            if(!empty($last_debit))
                $deductionsmade = true;
            
            $this->set("deductionsmade",$deductionsmade);
            
            //Get list of leases:
            $leases = $this->Lease->getLeases($tenant["Tenant"]["id"],$selected_month);
            $this->set('leases', $leases);
          
            //Get date of oldest lease for month selection. 
            //This gives us a range of possible bills
            $this->Lease->recursive = -1;
            $lease = $this->Lease->find('all',
                 array( 'conditions' => array( 'Lease.tenant_id' => $tenant["Tenant"]["id"]),
                         'order by' => 'Lease.date_from ASC',
                         'limit' => 1
                 )
            );
            //Work out an array of dates that leasese should exist.
            $dates = array();
            $first_date = $lease[0]['Lease']['date_from'];
            $ts_month =     mktime(0, 0, 0, date('n'), 1); //first day of this month (to get the last month)
            $ts_first = strtotime($first_date);
                         
            $amonth = $ts_first;
            do {
                $dates[date('Y-m',$amonth)] = date('Y-m',$amonth);
                $amonth = strtotime("+1 month",$amonth);
            } while( $amonth <= $ts_month);
            $dates[date('Y-m',$amonth)] = date('Y-m',$amonth);
            
            $this->set('listed_dates', $dates);
            // existing balance:
		$balance_cost = array(
			"detail" => "Outstanding Balance",
			"units" => "", 
			"cost_per_unit" => "",
			"cost" => $tenant["Tenant"]["balance"],
			"link" => "" 
		);
            //Build costs array:
            $costs = array();
            if( !$deductionsmade ) {
                //add monthly costs to array:
                $costs[0] = $balance_cost;

		// add existing balance:
                $costs = array_merge($costs,$this->getMonthlyCosts($id, $selected_month));

                //add lease costs to array:
                $costs = array_merge($costs,$this->getLeaseCosts($id, $selected_month));
            } else {
                //fetch transactions for the month.
            }
            //work out the total cost:
            $totalcost = 0;
            foreach( $costs as $acost)
                $totalcost += $acost["cost"];
                       
            $this->set("costs",$costs);
            $this->set("totalcost",$totalcost);
            
            //set pdf rendering settings:
            $this->pdfConfig  = array(
                'filename' => 'Account_'.$tenant['Tenant']['name'].'_'.$tenant['Tenant']['surname'].'_'.$selected_month.'.pdf',
                'engine' => 'CakePdf.DomPdf',
                'options' => array(
                    'print-media-type' => false,
                    'outline' => true,
                    'dpi' => 96
                ),
                'margin' => array(
                    'bottom' => 15,
                    'left' => 15,
                    'right' => 15,
                    'top' => 15
                ),
                'orientation' => 'portrait',
                'download' => true
            );
	}
        
	public function email($id=null) {
            $this->Tenant->id = $id;

            if (!$this->Tenant->exists()) {
                    throw new NotFoundException(__('Invalid tenant'));
            }
            $tenant = $this->Tenant->read(null, $id);

    /*      if ($this->request->is('post')) {

                    $email = new CakeEmail();
                    $email->from(array('pieter@dilectum.co.za' => 'Pieter Wagener'));
                    $to = explode(",",$tenant["Tenant"]["email"]);
                    //debug($to);
                    $email->to( $to );
                    $email->cc( 'pieterjohanneswagener@gmail.com');
                    $email->subject( 'Account for '.date('F_Y').'( '.$tenant['Tenant']['name'].' '.$tenant['Tenant']['surname'].' )' );
                    $email->attachments(array( $pdfname => $filename));
                    $email->send('Please find attached account for '.date('F_Y').'.');
                    $this->Session->setFlash(__('Email has been sent'));
                    $this->redirect(array('action' => 'view', $id));
            } */
	}

	  
	public function add() {
		
		if ($this->request->is('post')) {
			$this->Tenant->create();
			$data = $this->request->data;
			$data["Tenant"]["balance"] = 0;

			if ($this->Tenant->save( $data )) {
				$this->Session->setFlash(__('The tenant has been saved'));
				$this->redirect( array('controller'=> 'tenants', 'action' => 'view', $this->Tenant->getLastInsertID())  ); 
			} else {
				$this->Session->setFlash(__('The tenant could not be saved. Please, try again.'));
				$this->redirect(array('tenant' => 'add',));
			}
	
		}
		//$units = $this->Tenant->Unit->find('list');
		//$units = $this->Tenant->Unit->getUnits();
		//debug($units);
		$this->set(compact('units'));
	}

	public function add_transactions($id = null, $selected_month=null) {
            Controller::loadModel('Transaction');
            Controller::loadModel('Tenant');
            //Build costs array:
            $costs = array();

            //add lease costs to array:
            $costs = array_merge($costs,$this->getLeaseCosts($id,$selected_month));

            //add monthly costs to array:
            $costs = array_merge($costs,$this->getMonthlyCosts($id,$selected_month));

            $transactions = array();
            foreach( $costs as $acost ) {
                $trans = array();
                $trans["detail"] = $acost["detail"];
                $trans["cost_per_unit"] = $acost["cost_per_unit"];   
                $trans["units"] = $acost["units"];
                $trans["cost"] = $acost["cost"];
                $trans["tenant_id"] = $id;
                $trans["transaction_type"] = -1; 
                $trans["date"] = $selected_month."-1";
                $transaction["Transaction"] = $trans;
                $transactions[] = $transaction;
            }

            $this->Transaction->create();
            if ( $this->Transaction->saveAll($transactions) ) {
                $this->Session->setFlash('Transactions recorded.'); 

	    	//work out the total cost:
	    	$totalcost = 0;
	    	foreach( $costs as $acost) {
			$totalcost += $acost["cost"];
		}

		$tenant = $this->Tenant->read(null,$id);
		$tenant["Tenant"]["balance"] = $this->Tenant->balance+$totalcost; 
		$this->Tenant->save($tenant);
            } else {
                $this->Session->setFlash('Transactions not saved');
            }
            
            $this->redirect( array('controller'=> 'tenants', 'action' => 'view', $id, $selected_month ) );
        }

	public function edit($id = null) {
                Controller::loadModel('Unit');
		$this->Tenant->id = $id;

		if (!$this->Tenant->exists()) {
			throw new NotFoundException(__('Invalid tenant'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Tenant->save($this->request->data)) {
				$this->Session->setFlash(__('The tenant has been saved'));
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tenant could not be saved. Please, try again.'));
			}
		} else {
                    $this->request->data = $this->Tenant->read(null, $id);
                }
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Tenant->id = $id;
		if (!$this->Tenant->exists()) {
			throw new NotFoundException(__('Invalid tenant'));
		}
		if ($this->Tenant->delete()) {
			$this->Session->setFlash(__('Tenant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tenant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
