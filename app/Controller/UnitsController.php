<?php
App::uses('AppController', 'Controller');
/**
 * Units Controller
 *
 * @property Unit $Unit
 */
class UnitsController extends AppController {

    public function getByProperty() {
        $property_id = $this->request->data['Meter']['property_id'];

        $units = $this->Unit->find('list', array(
            'conditions' => array('property_id' => $property_id),
            'recursive' => -1
            ));
 
        $this->set('units',$units);
        $this->layout = 'ajax';
    }

	public function index() {
		$this->Unit->recursive = 0;
		//debug($this->paginate());
		$this->set('units', $this->paginate());
	}

	public function view($id = null) {
		$this->Unit->id = $id;
		if (!$this->Unit->exists()) {
			throw new NotFoundException(__('Invalid unit'));
		}
		$this->set('unit', $this->Unit->read(null, $id));
	}

	public function add($propertyid=null) {
		if($propertyid != null)
			$this->set('selected_id',$propertyid);
		if ($this->request->is('post')) {
			$this->Unit->create();
			if ($this->Unit->save($this->request->data)) {
            

				$this->Session->setFlash(__('The unit has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The unit could not be saved. Please, try again.'));
			}
		}
		$properties = $this->Unit->Property->find('list');
		$this->set(compact('properties'));
	}

/**
 * edit method
 *
 * @param string $id which unit to edit
 * @return void
 */
	public function edit($id = null) {
		$this->Unit->id = $id;
		if (!$this->Unit->exists()) {
			throw new NotFoundException(__('Invalid unit'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Unit->save($this->request->data)) {
				$this->Session->setFlash(__('The unit has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The unit could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Unit->read(null, $id);
		}
		$properties = $this->Unit->Property->find('list');
		$this->set(compact('properties'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Unit->id = $id;
		if (!$this->Unit->exists()) {
			throw new NotFoundException(__('Invalid unit'));
		}
		if ($this->Unit->delete()) {
			$this->Session->setFlash(__('Unit deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Unit was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
