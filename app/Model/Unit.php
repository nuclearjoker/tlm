<?php
App::uses('AppModel', 'Model');
/**
 * Unit Model
 *
 * @property House $House
 * @property Tenant $Tenant
 */
class Unit extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $belongsTo = array(
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
      
   public $hasMany = array(
        /*
            'Lease' => array(
            'className' => 'Lease',
            'foreignKey' => 'lease_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
        */
    );


	 public function getUnits() {
	     $this->recursive=1;
		$r = $this->find('all');
		$return = array();
		$i=0;
		 foreach($r as $unit) {
			$return[$unit["Unit"]["id"]] = $unit["Unit"]["name"]."   | ".$unit["Property"]["name"]." ";
			$i++;
		}
		return $return;
	
         }
}
		
