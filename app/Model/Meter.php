<?php

class Meter extends AppModel {
    public $actsAs = array( 'Containable' );
    
    public $useTable = 'meters';
    public $name = "Meter";

    public $belongsTo = array( 
                           'Property',
                           'Unit',
                           'UtilityType'
            ); 

    public $hasMany = array('Reading');

    
    
    public function getProperties($id=null) {
        $this->recursive=0;
        $conditions = array();
        $r = $this->Property->find('all', $conditions );
          $return = array();
          $i=0;
          foreach($r as $property) {
            $return[$property["Property"]["id"]] = $property["Property"]["name"];
            $i++;
          }
          return $return;
    }

    public function getMeters($selectbox=false,$prop_id=null) {
        $this->recursive=1;
        
        $cond =array();
        
        if(!empty($prop_id))
            $cond = array('Meter.property_id' => $prop_id);
        
        $r = $this->find('all', $cond);
        
        if(!$selectbox) //for selectbox.
            return $r;
        
        $return = array();
        $i=0;
        
        foreach($r as $meter) {
            if(!empty($meter["Unit"]["name"]))
                         $unitname = ", ".$meter["Unit"]["name"];
            $return[$meter["Meter"]["id"]] = $meter['UtilityType']['name'].' - '.$meter["Meter"]["name"]." (".$meter["Property"]["name"].$unitname.")";
        }
          
        return $return;
    }
    

    
}
?>
