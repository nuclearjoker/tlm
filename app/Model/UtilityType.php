<?php
App::uses('AppModel', 'Model');
/**
 * UtilityType Model
 *
 * @property MonthlyCost $MonthlyCost
 */
class UtilityType extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'displayfield';

    public $virtualFields = array(
        'displayfield' => "CONCAT(UtilityType.name,' - ',UtilityType.unit)"
    );

    public $validate = array(
/*        'name' => array(
            'rule' => 'alphanumeric',
            'required' => true,
            'message' => 'Alphanumeric values only.'
        ), */
        'costperunit' => array(
            'rule' => 'numeric',
            'required' => true,
            'message' => 'Number values only.'
        ),
        'unit' => array(
            'rule' => 'alphanumeric',
            'allowEmpty' => true,
            'message' => 'Alphanumeric values only.'
        )
    );

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MonthlyCost' => array(
			'className' => 'MonthlyCost',
			'foreignKey' => 'utility_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}

