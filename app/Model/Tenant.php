<?php
App::uses('AppModel', 'Model');
/**
 * Tenant Model
 *
 * @property Unit $Unit
 * @property MonthlyCost $MonthlyCost
 */
class Tenant extends AppModel {
    public $actsAs = array( 'Containable' );
/**
 * Display field
 *
 * @var string
 */
    public $virtualFields = array(
        'full_name' => "CONCAT(Tenant.name,' ', Tenant.surname)"
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

/*
	public $belongsTo = array(
		'Lease' => array(
			'className' => 'Lease',
			'foreignKey' => 'lease_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
*/

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MonthlyCost' => array(
			'className' => 'MonthlyCost',
			'foreignKey' => 'tenant_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Lease' => array(
			'className' => 'Lease',
			'foreignKey' => 'tenant_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    // shared function used for select boxes.
    public function getTenants() {
        $this->recursive = -1;
        $tenants = $this->find('all');

        $tenantsarr = array();
        foreach( $tenants as $tenant) {
            $viewstr = $tenant["Tenant"]["name"]." ".$tenant["Tenant"]["surname"];
            if ( !empty($tenant["Tenant"]["id_number"]) ) {
                $viewstr .= " (".$tenant["Tenant"]["id_number"].")";
            }
            $tenantsarr[ $tenant["Tenant"]["id"] ] = $viewstr;
        }

      return $tenantsarr;
    }
  
    /* @brief get Meters based on property_id 's
     * accepts single propertyid, or
     * array of multiple ids.
     * @param $isselect wether to output select box compatible array.
     */
    public function getMeters($tenant_id=null,$isselect=null) {
        if( $tenant_id == null)
            return null;

        $cond = array( 'Tenant.id' => $tenant_id);
                
        //$this->recursive = 2;
        $r = $this->find('first', 
                      array( 
                        'conditions' => $cond,
                        'contain' => array( 'Lease' => 
                                        array('Unit' => 
                                           array('Property' =>
                                             array('Meter' => 
                                                    array( 
                                                            'Property'=>array('Unit'),
                                                            'UtilityType'
                                                        ) 
                                                    ) 
                                                 ) 
                                             ) 
                                         )
                      )
                );
                
            
        //check if empty: 
        if (empty($r['Lease'][0]['Unit']))
            return array();

        $meters = array();
        foreach($r['Lease'] as $lease){
            foreach($lease['Unit']['Property']['Meter'] as $submeter){
                $meter['Meter'] = $submeter;
                //debug($submeter);
                if( $isselect == null) {
                   $meters = array_merge($meters, $submeter);
                }else if($isselect == true) {
                    if(!empty($lease["Unit"]["name"]))
                         $unitname = ", ".$lease["Unit"]["name"];
                    $meters[$meter["Meter"]["id"]] = $meter['Meter']['UtilityType']['name'].' - '.$meter["Meter"]["name"]." (".$meter["Meter"]["Property"]["name"].$unitname.")";
                }
            }
        }
        
        return $meters;
    }
  
}
