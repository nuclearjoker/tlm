<?php
class Reading extends AppModel {
  public $name = "Reading";

  public $belongsTo = array(
        'Meter' => array(
            'className' => 'Meter',
            'foreignKey' => 'meter_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
  );
  
    /*
     * @brief Get reading  from specific meter - for a specific month.
     * Assumuption for now is that there is only one reading per month.
     * @param meter_id
     * @param date  The date in the format Y-m.
     * @param date Wether to return the date of the reading as well.
     */
    public function getReading($meter_id=null,$date=null,$return_date=false) {
        if( $meter_id==null || $date==null)
            return 0;
             
        $this->recursive=-1;
        $cond = array( 'Reading.meter_id' => $meter_id);
        
        //date one month ago.
        
        $beginning =$date.'-01';
        $end = $date.'-31';
        if( !empty($date) ) {
           $cond = array_merge($cond, 
                    array('Reading.for_month BETWEEN ? AND ?' => array(  $beginning, $end ) ) 
                   );
        }
        
        $r = $this->find('first', array(
            'conditions' => $cond,
            'order' => array( 'for_month DESC'),
            'limit' => 1
        ));
        
        
        if(!empty($r["Reading"])) {
            if($return_date) {
                $return["units"] = $r["Reading"]["units"];
                $return["for_month"] = $r["Reading"]["for_month"];
            } else {
                $return = $r["Reading"]["units"];
            }
        } else {
            $return = 0;
        }
        return $return;
    }
    /*
     * @brief Get reading  from specific meter - for a specific month.
     * @note Assumuption for now is that there is only one reading per month.
     * @param meter_id id of the meter.
     * @param date Wether to return the date of the reading as well.
     */
    public function getLastReading($meter_id=null,$return_date=false) {
        if( $meter_id==null)
            return 0;
             
        $this->recursive=-1;
        $cond = array( 'Reading.meter_id' => $meter_id);
       
        $r = $this->find('first', array(
            'conditions' => $cond,
            'order' => array( 'for_month DESC'),
            'limit' => 1
        ));
        
        if(!empty($r["Reading"])) {
            if($return_date) {
                $return["units"] = $r["Reading"]["units"];
                $return["for_month"] = $r["Reading"]["for_month"];
            } else {
                $return = $r["Reading"]["units"];
            }
        } else {
            $return = 0;
        }
        
        return $return;
    }
    
    /*
     * Get usage for a particular month.
     */
    public function getUsage($meter_id, $date=null) {
        if( $meter_id==null || $date==null)
            return 0;

        $datearr = split("-",$date);
        $Y = $datearr[0];
        $m = $datearr[1];
        
        $datelast = date('Y-m', strtotime('-1 month', mktime( 0, 0, 0 , $m, 1, $Y ))  );
        
        $reading_now = $this->getReading($meter_id,$date);
        $reading_last = $this->getReading($meter_id,$datelast);
        
        if( $reading_now >= $reading_last )
            return $reading_now-$reading_last;
        else
            return 0;
    }
}
?>
