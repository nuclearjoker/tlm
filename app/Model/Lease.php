<?php
App::uses('AppModel', 'Model');

class Lease extends AppModel {
	public $displayField = 'name';

	public $belongsTo = array(
        'LeaseType',
        'Tenant',
        'Unit'
	);
    
    public $validate = array(
        'due' => array( 
            'rule' => 'numeric',
            'required' => 'true',
            'message' => 'Number values only.'
        ),
        'date_from'  => array(
            'rule' => 'date',
            'required' => 'true',
            'message' => 'Date format of YYYY-MM-DD only.'
        ),
        'date_to'  => array(
            'rule' => 'date',
            'required' => 'true',
            'message' => 'Date format of YYYY-MM-DD only.'
        ),
        'deposit'  => array(
            'rule' => 'numeric',
            'required' => 'true',
            'message' => 'Number values only.'
        ),
    );
    
    public function getLeases($tenantid,$selected_month=null) {
             if($selected_month==null)
                $selected_month=date('Y-m');
            
            //Get list of leases:
            $sql_first_day = $selected_month."-1";  //first day of selected month.
            $sql_last_day = $selected_month.'-'.date('t', strtotime($selected_month)); //last day of selected month.
            $this->recursive = 2;
            $leases = $this->find('all',
                     array( 'conditions' => array( 'Lease.tenant_id' => $tenantid,
                        'Lease.date_from <=' => $sql_first_day,
                        'Lease.date_to >=' => $sql_last_day
                     )
                   )
                 );
            return $leases;
    }
}

