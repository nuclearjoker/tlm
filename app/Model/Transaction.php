<?php
App::uses('AppModel', 'Model');
/**
 * Transaction Model
 *
 * @property Tenant $Tenant
 */
class Transaction extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'detail';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tenant' => array(
			'className' => 'Tenant',
			'foreignKey' => 'tenant_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
/*
 * @brief get the last transaction a tenant made.
 * @param transact_type 1 for credit, -1 for debit
 */
        public function lastTransaction($tenant_id, $transact_type=1) {
            $ret = $this->find('all', 
                            array( 'conditions' => 
                                array( 'Transaction.tenant_id'=>$tenant_id, 
                                    'Transaction.transaction_type' => $transact_type
                                 ),
                                'recursive' => -1,
                                'order' => array('Transaction.date'=>'DESC'), 
                                'limit' => 1
                               )
                         );
            return $ret;
        }
        public function lastDebit($id, $selected_date) {
            $beginning =$selected_date.'-01';
            $end = $selected_date.'-31';
     
            $r = $this->find('all', array( 'conditions' => array( 
                                        'Transaction.tenant_id' => $id,
                                        'Transaction.transaction_type' => -1,
                                        'Transaction.date BETWEEN ? AND ?' => array(  $beginning, $end ) 
                                    ),
                                    'recursive' => -1,
                                    'order' => array('Transaction.date'=>'DESC'), 
                                    'limit' => 1
                                    )
                              );
            return $r;
        }
}
