<?php
App::uses('AppModel', 'Model');
class MeterType extends AppModel {
	public $displayField = 'name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $hasMany = array(
		'Meter' => array(
			'className' => 'Meter',
			'foreignKey' => 'meter_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
        public $hasOne = array(
            'UtilityType'
        );
}
