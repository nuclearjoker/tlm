<?php
App::uses('AppModel', 'Model');
/**
 * UtilityType Model
 *
 * @property MonthlyCost $MonthlyCost
 */
class LeaseType extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField ='display_type';

    public $virtualFields = array(
        'display_type' => "CONCAT(n_period,' ', period_type)"
    );
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Lease' => array(
			'className' => 'Lease',
			'foreignKey' => 'lease_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
