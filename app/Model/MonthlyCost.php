<?php
App::uses('AppModel', 'Model');
/**
 * MonthlyCost Model
 *
 * @property Meter $Meter
 * @property Utility $Utility
 * @property Tenant $Tenant
 * @property  $
 */
class MonthlyCost extends AppModel {
    public $actsAs = array( 'Containable' );
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'monthly_costs';
	
	public $displayField = 'MonthlyCost.name';

	public $belongsTo = array(
		'Meter' => array(
			'className' => 'Meter',
			'foreignKey' => 'meter_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'UtilityType' => array(
			'className' => 'UtilityType',
			'foreignKey' => 'utility_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tenant' => array(
			'className' => 'Tenant',
			'foreignKey' => 'tenant_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
      
      public $validate = array(
         'units' => array( 
            'rule' => 'numeric',
            'required' => 'true',
            'message' => 'Number values only.'
        ),
        'fraction_of_cost' => array(
            'rule' => 'numeric',
            'required' => 'true',
            'message' => 'Number values only.'
        ),
        'date_from'  => array(
        /*    //'rule' => array( 'datetime','Ym'),
            'required' => 'true',
            'message' => 'Date format of YYYY-MM only.' */
        ),
        'date_to'  => array(
         /*   //'rule' => array( 'datetime','Ym'),
            'required' => 'true',
            'message' => 'Date format of YYYY-MM only.' */
        ) 
    );
}
