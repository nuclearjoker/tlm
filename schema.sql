CREATE TABLE meters ( 
        id INT AUTO_INCREMENT PRIMARY KEY,
        modified DATETIME DEFAULT NULL,
        created DATETIME DEFAULT NULL,
        name VARCHAR(256),
        property_id INT,
        unit_id INT,
        utility_type_id INT,
        meter_type_id INT
    );

CREATE INDEX property_id_idx ON meters(property_id);
CREATE INDEX unit_id_idx ON meters(unit_id);
CREATE INDEX utility_type_id_idx ON meters(utility_type_id);
CREATE INDEX meter_type_id_idx ON meters(meter_type_id);

CREATE TABLE meter_types (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(128),
        unit VARCHAR(128)
    );

CREATE TABLE readings (
                id BIGINT AUTO_INCREMENT PRIMARY KEY,
                created DATETIME DEFAULT NULL,
                modified DATETIME DEFAULT NULL,
                for_month DATE DEFAULT NULL,
                date_taken DATE DEFAULT NULL,
                units DECIMAL(20,4) DEFAULT 0.0,
                meter_id INT
        );
CREATE INDEX meter_id_idx ON readings(meter_id);

CREATE TABLE properties (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    created DATETIME DEFAULT NULL,
                    modified DATETIME DEFAULT NULL,
                    name VARCHAR(256) NOT NULL,
                    active BOOL,
                    address VARCHAR(512),
                    latitude DOUBLE,
                    longitude DOUBLE,
                    picture VARCHAR(400)
                    );

CREATE TABLE onceoff_costs (
                       id INT AUTO_INCREMENT PRIMARY KEY,
                       created DATETIME DEFAULT NULL,
                       modified DATETIME DEFAULT NULL,
                       name VARCHAR(256) NOT NULL DEFAULT "",
                       for_date DATE DEFAULT NULL,
                       meter_id INT,
                       utility_type_id INT,
                       tenant_id INT,
                       fraction_of_cost DECIMAL(20,4) NOT NULL DEFAULT 1.0
                      );
CREATE INDEX meter_id_idx ON onceoff_costs(meter_id);
CREATE INDEX tenant_id_idx ON onceoff_costs(tenant_id);

CREATE TABLE monthly_costs (
                       id INT AUTO_INCREMENT PRIMARY KEY,
                       created DATETIME DEFAULT NULL,
                       modified DATETIME DEFAULT NULL,
                       name VARCHAR(256) NOT NULL DEFAULT "",
                       meter_id INT DEFAULT NULL,
                       utility_type_id INT DEFAULT NULL,
                       units INT DEFAULT NULL,
                       tenant_id INT DEFAULT NULL,
                       fraction_of_cost DECIMAL(20, 4) NOT NULL DEFAULT 1.0,
                       date_from DATE DEFAULT NULL,
                       date_to DATE DEFAULT NULL
                  );

CREATE INDEX meter_id_idx ON monthly_costs(meter_id);
CREATE INDEX tenant_id_idx ON monthly_costs(tenant_id);
CREATE INDEX date_from_idx ON monthly_costs(date_from);
CREATE INDEX date_to_idx ON monthly_costs(date_to);

CREATE TABLE tenants (
        id INT AUTO_INCREMENT PRIMARY KEY,
        created DATETIME DEFAULT NULL,
        modified DATETIME DEFAULT NULL,
        balance  DECIMAL(20,4) NOT NULL DEFAULT 0.0,
        name VARCHAR(256) NOT NULL DEFAULT "",
        surname VARCHAR(256),
        email  VARCHAR(512),
        phone VARCHAR(32),
        active BOOL
      );

CREATE TABLE responsible_persons (
        id INT AUTO_INCREMENT PRIMARY KEY,
        created DATETIME DEFAULT NULL,
        modified DATETIME DEFAULT NULL,
        name VARCHAR(256) NOT NULL DEFAULT "",
        surname VARCHAR(256),
        email  VARCHAR(512),
        phone VARCHAR(32),
        tenant_id INT,
        active BOOL
      );
CREATE INDEX tenant_id_idx ON responsible_persons(tenant_id);

CREATE TABLE lease_types (
    id INT AUTO_INCREMENT PRIMARY KEY,
	description VARCHAR(256),
	period_type VARCHAR(256) DEFAULT "MONTHLY",
    n_period INT
);

INSERT INTO lease_types(description,period_type,n_period) VALUES("Monthly lease, payed every month.","MONTHLY",1);
INSERT INTO lease_types(description,period_type,n_period) VALUES("Yearly lease, payed every year.","YEARLY",1);
INSERT INTO lease_types(description,period_type,n_period) VALUES("Yearly lease, payed every 2 years.","YEARLY",2);
INSERT INTO lease_types(description,period_type,n_period) VALUES("Monthly lease, payed every 3 months.","MONTHLY",3);

CREATE TABLE leases (
        id INT AUTO_INCREMENT PRIMARY KEY,
        created DATETIME DEFAULT NULL,
        modified DATETIME DEFAULT NULL,
        date_from DATE DEFAULT NULL,
        date_to DATE DEFAULT NULL,
	    lease_type_id INT,
        due  	 DECIMAL(20,4) NOT NULL DEFAULT 0.0,
        deposit  DECIMAL(20,4) NOT NULL DEFAULT 0.0,
	    payed_deposit BOOL DEFAULT FALSE,
	    returned_deposit BOOL DEFAULT FALSE,
        unit_id INT,
        tenant_id INT,
        active BOOL
      );
CREATE INDEX tenant_id_idx ON leases(tenant_id);
CREATE INDEX unit_id_idx ON leases(unit_id);

CREATE TABLE transactions (id INT AUTO_INCREMENT PRIMARY KEY,
 created DATETIME DEFAULT NULL,
 modified DATETIME DEFAULT NULL,
 transaction_type INT,
 cost DECIMAL(20,4) NOT NULL DEFAULT 0.00,
 tenant_id INT,
 detail VARCHAR(256),
 units DECIMAL(20,4) NOT NULL DEFAULT 0.00,
 cost_per_unit DECIMAL(20,4) NOT NULL DEFAULT 0.00,
 `date` DATE DEFAULT NULL
);
CREATE INDEX tenant_id_idx ON transactions(tenant_id);
CREATE INDEX transaction_type_idx ON transactions(transaction_type);
CREATE INDEX date_idx ON transactions(date);

CREATE TABLE units (
 id INT AUTO_INCREMENT PRIMARY KEY,
 created  DATETIME DEFAULT NULL,
 modified DATETIME DEFAULT NULL,
 rent DECIMAL (20,4) NOT NULL DEFAULT 0.00,
 name VARCHAR(256) NOT NULL,
 property_id INT NOT NULL,
 active BOOL
);
CREATE INDEX property_id_idx ON units(property_id);

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    modified DATETIME DEFAULT NULL,
    created DATETIME NOT NULL,
    group_id INT NOT NULL,
    role VARCHAR(40),
    username VARCHAR(256) NOT NULL,
    password VARCHAR(40)
);

CREATE INDEX group_id_idx ON users(group_id);
CREATE TABLE groups (
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    created DATETIME,
    modified DATETIME
);

INSERT INTO groups(name) VALUES( "admin");
INSERT INTO users(group_id,username,password) VALUES(1,'jacques','141244d6759b568daf4a234b4618f35e988017f2');
INSERT INTO users(group_id,username,password) VALUES(1,'pieter','4f757919d2485959d261ee41744338d8fe5c6873');

CREATE TABLE utility_types (
    id          INT AUTO_INCREMENT PRIMARY KEY AUTO_INCREMENT,
    created     DATETIME DEFAULT NULL,
    modified    DATETIME DEFAULT NULL,
    name        VARCHAR(256) NOT NULL,
    costperunit DECIMAL(20,4) NOT NULL DEFAULT 0.00,
    unit        VARCHAR(10),
    unit_description VARCHAR(256)
);
